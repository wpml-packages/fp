<?php

use tad\FunctionMocker\FunctionMocker;

define( 'MAIN_DIR', __DIR__ . '/..' );

require_once MAIN_DIR . '/vendor/antecedent/patchwork/Patchwork.php';
require_once MAIN_DIR . '/tests/testable-classes/TestObject.php';

include MAIN_DIR . '/vendor/autoload.php';

FunctionMocker::init( [ 'blacklist' => dirname( __DIR__ ) ] );

FunctionMocker::replace( '\WPML\Container\make', 'false ' );


