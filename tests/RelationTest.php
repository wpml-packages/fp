<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class RelationTest extends TestCase {

	/**
	 * @test
	 */
	public function test_equals() {
		$this->assertTrue( Relation::equals( 1, 1 ) );
		$this->assertFalse( Relation::equals( 1, '1' ) );
		$this->assertTrue( Relation::equals( [ 1, 2, 3 ], [ 1, 2, 3 ] ) );
	}

	/**
	 * @test
	 */
	public function test_less_than() {
		$this->assertFalse( Relation::lt( 2, 1 ) );
		$this->assertFalse( Relation::lt( 2, 2 ) );
		$this->assertTrue( Relation::lt( 2, 3 ) );

		$this->assertFalse( Relation::lt( 2, 1.0 ) );
		$this->assertFalse( Relation::lt( 2.0, 2 ) );
		$this->assertTrue( Relation::lt( 2.0, 2.1 ) );

		$this->assertTrue( Relation::lt( 'a', 'z' ) );
		$this->assertFalse( Relation::lt( 'z', 'a' ) );

		$this->assertTrue( Relation::lt( 'anaconda', 'zoo' ) );
		$this->assertFalse( Relation::lt( 'zoo', 'anaconda' ) );

		$this->assertFalse( Relation::lt( (object) [ 'v' => 2 ], (object) [ 'v' => 1 ] ) );
		$this->assertFalse( Relation::lt( (object) [ 'v' => 2 ], (object) [ 'v' => 2 ] ) );
		$this->assertTrue( Relation::lt( (object) [ 'v' => 2 ], (object) [ 'v' => 3 ] ) );
	}

	/**
	 * @test
	 */
	public function test_less_than_or_equal() {
		$this->assertFalse( Relation::lte( 2, 1 ) );
		$this->assertTrue( Relation::lte( 2, 2 ) );
		$this->assertTrue( Relation::lte( 2, 3 ) );

		$this->assertFalse( Relation::lte( 2, 1.0 ) );
		$this->assertTrue( Relation::lte( 2.0, 2 ) );
		$this->assertTrue( Relation::lte( 2.0, 2.1 ) );

		$this->assertTrue( Relation::lte( 'a', 'z' ) );
		$this->assertTrue( Relation::lte( 'a', 'a' ) );
		$this->assertFalse( Relation::lte( 'z', 'a' ) );

		$this->assertTrue( Relation::lte( 'anaconda', 'zoo' ) );
		$this->assertTrue( Relation::lte( 'anaconda', 'anaconda' ) );
		$this->assertFalse( Relation::lte( 'zoo', 'anaconda' ) );

		$this->assertFalse( Relation::lte( (object) [ 'v' => 2 ], (object) [ 'v' => 1 ] ) );
		$this->assertTrue( Relation::lte( (object) [ 'v' => 2 ], (object) [ 'v' => 2 ] ) );
		$this->assertTrue( Relation::lte( (object) [ 'v' => 2 ], (object) [ 'v' => 3 ] ) );
	}

	/**
	 * @test
	 */
	public function test_greater_than() {
		$this->assertFalse( Relation::gt( 1, 2 ) );
		$this->assertFalse( Relation::gt( 2, 2 ) );
		$this->assertTrue( Relation::gt( 3, 2 ) );

		$this->assertFalse( Relation::gt( 1.0, 2 ) );
		$this->assertFalse( Relation::gt( 2, 2.0 ) );
		$this->assertTrue( Relation::gt( 2.1, 2.0 ) );

		$this->assertTrue( Relation::gt( 'z', 'a' ) );
		$this->assertFalse( Relation::gt( 'a', 'z' ) );

		$this->assertTrue( Relation::gt( 'zoo', 'anaconda' ) );
		$this->assertFalse( Relation::gt( 'anaconda', 'zoo' ) );

		$this->assertFalse( Relation::gt( (object) [ 'v' => 1 ], (object) [ 'v' => 2 ] ) );
		$this->assertFalse( Relation::gt( (object) [ 'v' => 2 ], (object) [ 'v' => 2 ] ) );
		$this->assertTrue( Relation::gt( (object) [ 'v' => 3 ], (object) [ 'v' => 2 ] ) );
	}

	/**
	 * @test
	 */
	public function test_greater_than_or_equal() {
		$this->assertFalse( Relation::gte( 1, 2 ) );
		$this->assertTrue( Relation::gte( 2, 2 ) );
		$this->assertTrue( Relation::gte( 3, 2 ) );

		$this->assertFalse( Relation::gte( 1.0, 2 ) );
		$this->assertTrue( Relation::gte( 2, 2.0 ) );
		$this->assertTrue( Relation::gte( 2.1, 2.0 ) );

		$this->assertTrue( Relation::gte( 'z', 'a' ) );
		$this->assertTrue( Relation::gte( 'z', 'z' ) );
		$this->assertFalse( Relation::gte( 'a', 'z' ) );

		$this->assertTrue( Relation::gte( 'zoo', 'anaconda' ) );
		$this->assertTrue( Relation::gte( 'zoo', 'zoo' ) );
		$this->assertFalse( Relation::gte( 'anaconda', 'zoo' ) );

		$this->assertFalse( Relation::gte( (object) [ 'v' => 1 ], (object) [ 'v' => 2 ] ) );
		$this->assertTrue( Relation::gte( (object) [ 'v' => 2 ], (object) [ 'v' => 2 ] ) );
		$this->assertTrue( Relation::gte( (object) [ 'v' => 3 ], (object) [ 'v' => 2 ] ) );
	}

	/**
	 * @test
	 */
	public function test_propEq() {
		$abby         = [ 'name' => 'Abby', 'age' => 7, 'hair' => 'blond' ];
		$fred         = [ 'name' => 'Fred', 'age' => 12, 'hair' => 'brown' ];
		$rusty        = [ 'name' => 'Rusty', 'age' => 10, 'hair' => 'brown' ];
		$alois        = [ 'name' => 'Alois', 'age' => 15, 'disposition' => 'surly' ];
		$kids         = [ $abby, $fred, $rusty, $alois ];
		$hasBrownHair = Relation::propEq( 'hair', 'brown' );
		$this->assertEquals( [ $fred, $rusty ], Fns::filter( $hasBrownHair, $kids ) );
	}

	/**
	 * @test
	 */
	public function test_sortWith() {
		$sample = [
			[ 'name' => "Sally", 'age' => 29, 'height' => 65 ],
			[ 'name' => "Zac", 'age' => 29, 'height' => 72 ],
			[ 'name' => "John", 'age' => 32, 'height' => 61 ],
			[ 'name' => "Lisa", 'age' => 28, 'height' => 63 ],
			[ 'name' => "Bob", 'age' => 29, 'height' => 66 ],
			[ 'name' => "Allen", 'age' => 29, 'height' => 66 ],
		];

		$result = Relation::sortWith( [
			Fns::descend( Obj::prop( 'height' ) ),
			Fns::ascend( Obj::prop( 'age' ) ),
			Fns::ascend( Obj::prop( 'name' ) )
		], $sample );

		$this->assertEquals(
			[
				[ 'name' => "Zac", 'age' => 29, 'height' => 72 ],
				[ 'name' => "Allen", 'age' => 29, 'height' => 66 ],
				[ 'name' => "Bob", 'age' => 29, 'height' => 66 ],
				[ 'name' => "Sally", 'age' => 29, 'height' => 65 ],
				[ 'name' => "Lisa", 'age' => 28, 'height' => 63 ],
				[ 'name' => "John", 'age' => 32, 'height' => 61 ],
			],
			$result
		);

	}
}
