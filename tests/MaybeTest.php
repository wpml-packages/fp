<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;
use WPML\FP\Str;

class MaybeTest extends TestCase {

	/**
	 * @test
	 */
	public function it_creates_just() {
		$value = 'something';

		$just = Maybe::just( $value );
		$this->assertEquals( $value, $just->get() );
		$this->assertTrue( $just->isJust() );
		$this->assertFalse( $just->isNothing() );
	}

	/**
	 * @test
	 */
	public function it_creates_just_from_nullable() {
		$value = 'something';

		$just = Maybe::fromNullable( $value );
		$this->assertEquals( $value, $just->get() );
	}

	/**
	 * @test
	 */
	public function it_creates_nothing() {
		$nothing = Maybe::nothing();
		$this->assertTrue( $nothing->isNothing() );
		$this->assertFalse( $nothing->isJust() );
	}

	/**
	 * @test
	 */
	public function it_creates_nothing_from_nullable() {
		$nothing = Maybe::fromNullable( null );
		$this->assertTrue( $nothing->isNothing() );
	}

	/**
	 * @test
	 */
	public function it_creates_just_from_of() {
		$value = 'something';

		$just = Maybe::of( $value );
		$this->assertEquals( $value, $just->get() );
		$this->assertTrue( $just->isJust() );
	}

	/**
	 * @test
	 */
	public function it_returns_result_from_pipeline() {

		$value = 'something';

		$strLengthGreaterThenFive = function ( $str ) {
			return strlen( $str ) > 5;
		};

		$result = Maybe::fromNullable( $value )
		               ->filter( $strLengthGreaterThenFive )
		               ->map( 'strtoupper' )
		               ->map( 'strrev' )
		               ->getOrElse( 'invalid string length' );

		$this->assertEquals( 'GNIHTEMOS', $result );
	}

	/**
	 * @test
	 */
	public function it_returns_failed_from_pipeline() {

		$value = 'some';

		$strLengthGreaterThenFive = function ( $str ) {
			return strlen( $str ) > 5;
		};

		$result = Maybe::fromNullable( $value )
		               ->filter( $strLengthGreaterThenFive )
		               ->map( 'strtoupper' )
		               ->map( 'strrev' )
		               ->getOrElse( 'invalid string length' );

		$this->assertEquals( 'invalid string length', $result );
	}

	/**
	 * @test
	 */
	public function test_maybe_safe() {
		$value = 123;
		$array = [ $value ];

		$safeNth = Maybe::safe( Lst::nth() );

		$this->assertEquals(
			Maybe::just( $value ),
			$safeNth( 0, $array )
		);

		$this->assertEquals(
			Maybe::nothing(),
			$safeNth( 1, $array )
		);
	}

	/**
	 * @test
	 */
	public function test_maybe_safeAfter() {
		$isSmall = Relation::lt( Fns::__, 10 );

		$double = Math::multiply( 2 );

		$safeDouble = Maybe::safeAfter( $isSmall, $double );

		$this->assertEquals(
			Maybe::just( 6 ),
			$safeDouble( 3 )
		);

		$this->assertEquals(
			Maybe::nothing(),
			$safeDouble( 7 )
		);
	}

	/**
	 * @test
	 */
	public function test_maybe_safeBefore() {
		$isSmall = Relation::lt( Fns::__, 10 );

		$double = Math::multiply( 2 );

		$safeDouble = Maybe::safeBefore( $isSmall, $double );

		$this->assertEquals(
			Maybe::just( 6 ),
			$safeDouble( 3 )
		);

		$this->assertEquals(
			Maybe::just( 14 ),
			$safeDouble( 7 )
		);

		$this->assertEquals(
			Maybe::nothing(),
			$safeDouble( 11 )
		);
	}

	/**
	 * @test
	 */
	public function it_rejects() {

		$strLengthGreaterThenFive = pipe( Str::len(), Relation::gt( Fns::__, 5 ) );

		$this->assertEquals(
			'TEST',
			Maybe::of( 'test' )
			     ->reject( $strLengthGreaterThenFive )
			     ->map( Str::toUpper() )
			     ->getOrElse( 'failed' )
		);

		$this->assertEquals(
			'failed',
			Maybe::of( 'longer string' )
			     ->reject( $strLengthGreaterThenFive )
			     ->map( Str::toUpper() )
			     ->getOrElse( 'failed' )
		);

		$this->assertEquals(
			Maybe::nothing(),
			Maybe::nothing()
				->reject( $strLengthGreaterThenFive )
		);


	}


}
