<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class EitherTest extends TestCase {

	/**
	 * @test
	 */
	public function it_creates_right_for_valid() {
		$this->assertInstanceOf( Right::class, Either::fromNullable( 'value' ) );
	}

	/**
	 * @test
	 */
	public function it_creates_left_for_null() {
		$this->assertInstanceOf( Left::class, Either::fromNullable( null ) );
	}

	/**
	 * @test
	 */
	public function it_creates_right_by_default() {
		$this->assertInstanceOf( Right::class, Either::of( 'value' ) );
	}

	/**
	 * @test
	 */
	public function it_gets_value() {
		$value = 'something';
		$this->assertEquals( $value, Either::of( $value )->get() );
	}

	/**
	 * @test
	 */
	public function filter_returns_value_on_true() {
		$right    = Either::right( 2 );
		$filtered = $right->filter( function ( $x ) {
			return $x > 1;
		} );
		$this->assertEquals(
			$right->get(),
			$filtered->get()
		);
		$this->assertInstanceOf( Right::class, $filtered );
	}

	/**
	 * @test
	 */
	public function filter_returns_left_on_false() {
		$right    = Either::right( 0 );
		$filtered = $right->filter( function ( $x ) {
			return $x > 1;
		} );
		$this->assertInstanceOf( Left::class, $filtered );

		$this->assertEquals( 0, $filtered->orElse( Fns::identity() )->get() );
	}

	/**
	 * @test
	 */
	public function it_joins() {
		$x      = 10;
		$right  = Either::of( Either::of( $x ) );
		$joined = $right->join();
		$this->assertInstanceOf( Right::class, $joined );
		$this->assertEquals( 10, $joined->get() );
	}

	/**
	 * @test
	 */
	public function it_chains() {
		$x      = 10;
		$right  = Either::of( $x );
		$fn     = function ( $value ) { return Either::of( $value ); };
		$result = $right->chain( $fn );
		$this->assertInstanceOf( Right::class, $result );
		$this->assertEquals( 10, $result->get() );
	}

	/**
	 * @test
	 */
	public function it_returns_right_on_try_success() {
		$invert = function ( $v ) { return 1 / $v; };
		$x      = 10;
		$this->assertEquals( 1 / $x, Right::of( $x )->tryCatch( $invert )->get() );
	}

	/**
	 * @test
	 */
	public function it_returns_left_on_try_error() {
		$invert = function ( $v ) { return 1 / $v; };
		$x      = 0;
		Right::of( $x )->tryCatch( $invert )->orElse( function ( \Exception $e ) {
			$this->assertEquals( 'Division by zero', $e->getMessage() );
		} );
	}

	/**
	 * @test
	 */
	public function test_bimap() {
		$leftFn  = function ( $str ) {
			return sprintf( "<span class='error' >%s</span>", $str );
		};
		$rightFn = function ( $str ) {
			return sprintf( "<span class='success' >%s</span>", $str );
		};

		$left = Either::left( 'error message' );
		$this->assertEquals(
			"<span class='error' >error message</span>",
			$left->bimap( $leftFn, $rightFn )->orElse( Fns::identity() )->get()
		);

		$right = Either::right( 'success result' );
		$this->assertEquals(
			"<span class='success' >success result</span>",
			$right->bimap( $leftFn, $rightFn )->getOrElse( '' )
		);

		$result = Either::of( 5 )->filter( Relation::gt( Fns::__, 10 ) )->bimap( $leftFn, $rightFn );
		$this->assertEquals(
			"<span class='error' >5</span>",
			$result->orElse( Fns::identity() )->get()
		);

		$result = Either::of( 15 )->filter( Relation::gt( Fns::__, 10 ) )->bimap( $leftFn, $rightFn );
		$this->assertEquals(
			"<span class='success' >15</span>",
			$result->get()
		);
	}

	/**
	 * @test
	 */
	public function test_bichain() {
		$left = Either::left( 'invalid value' );
		$this->assertEquals(
			Either::right( 'invalid value' ),
			$left->bichain( Either::right(), Either::left() )
		);

		$right = Either::right( 'correct value' );
		$this->assertEquals(
			Either::left( 'correct value' ),
			$right->bichain( Either::right(), Either::left() )
		);

		$safeToUpper = function ( $param ) {
			return is_string( $param ) ? Either::of( strtoupper( $param ) ) : Either::left( $param );
		};

		$left   = Either::left( 123 );
		$result = $left->bichain( $safeToUpper, Either::of() );
		$this->assertEquals( Either::left( 123 ), $result );

		$left   = Either::left( 'error' );
		$result = $left->bichain( $safeToUpper, Either::of() );
		$this->assertEquals( Either::right( 'ERROR' ), $result );

		$right  = Either::right( 123 );
		$result = $right->bichain( Either::of(), $safeToUpper );
		$this->assertEquals( Either::left( 123 ), $result );

		$right  = Either::right( 'value' );
		$result = $right->bichain( Either::of(), $safeToUpper );
		$this->assertEquals( Either::right( 'VALUE' ), $result );

		$right  = Either::right( 'value' );
		$result = $right->bichain( Either::of(), Either::of() );
		$this->assertEquals( Either::right( 'value' ), $result );

		$left   = Either::left( 'error' );
		$result = $left->bichain( Either::left(), Either::right() );
		$this->assertEquals( Either::left( 'error' ), $result );
	}

	/**
	 * @test
	 */
	public function test_coalesce() {
		$leftFn  = function ( $str ) {
			return sprintf( "<span class='error' >%s</span>", $str );
		};
		$rightFn = function ( $str ) {
			return sprintf( "<span class='success' >%s</span>", $str );
		};

		$left = Either::left( 'error message' );
		$this->assertEquals(
			Either::of( "<span class='error' >error message</span>" ),
			$left->coalesce( $leftFn, $rightFn )
		);

		$right = Either::right( 'success result' );
		$this->assertEquals(
			Either::of( "<span class='success' >success result</span>" ),
			$right->coalesce( $leftFn, $rightFn )
		);

		$result = Either::of( 5 )->filter( Relation::gt( Fns::__, 10 ) )->coalesce( $leftFn, $rightFn );
		$this->assertEquals( Either::of( "<span class='error' >5</span>" ), $result );

		$result = Either::of( 15 )->filter( Relation::gt( Fns::__, 10 ) )->coalesce( $leftFn, $rightFn );
		$this->assertEquals( Either::of( "<span class='success' >15</span>" ), $result );
	}


	/**
	 * @test
	 */
	public function test_alt() {
		$actual = Either::right( 45 )->alt( Either::right( 97 ) )->alt( Either::left( 'error' ) );
		$this->assertEquals( Either::right( 45 ), $actual );

		$actual = Either::left( 'error' )->alt( Either::left( 'error 2' ) )->alt( Either::left( 'final error' ) );
		$this->assertEquals( Either::left( 'final error' ), $actual );

		$actual = Either::left( 'error' )->alt( Either::right( 95 ) )->alt( Either::left( 'final error' ) );
		$this->assertEquals( Either::right( 95 ), $actual );
	}

	/**
	 * @test
	 */
	public function it_handles_fromBool() {
		$falsy = [
			false,
			0,
			0.0,
			'',
			'0',
			[],
			null,
		];

		$checkFalse = function ( $value) {
			$this->assertEquals( Either::left( $value), Either::fromBool( $value ));
		};

		wpml_collect( $falsy )->map( $checkFalse );

		$truthy = [
			true,
			1,
			1.0,
			'abc',
			'1.0',
			[ 'abc'],
		];

		$checkTrue = function ( $value) {
			$this->assertEquals( Either::of( $value), Either::fromBool( $value ));
		};

		wpml_collect( $truthy )->map( $checkTrue );
	}

}
