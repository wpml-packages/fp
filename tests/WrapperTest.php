<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class WrapperTest extends TestCase {

	/**
	 * @test
	 */
	public function it_gets_value() {
		$value = 'some value';

		$wrapper = Wrapper::of( $value );
		$this->assertEquals( $value, $wrapper->get() );
	}

	/**
	 * @test
	 */
	public function it_maps_function() {
		$value = 'some value';

		$wrapper = Wrapper::of( $value );
		$this->assertEquals( strtoupper( $value ), $wrapper->map( 'strtoupper' )->get() );
		$this->assertEquals( $value, $wrapper->get(), 'Original should not be mutated' );
	}

	/**
	 * @test
	 */
	public function it_flatterns_with_join() {
		$value = 'some value';

		$wrapper = Wrapper::of( Wrapper::of( $value ) );
		$this->assertEquals( $value, $wrapper->join() );
	}

	/**
	 * @test
	 */
	public function it_filters() {
		$value = 'some value';

		$startsWith = function ( $needle ) {
			$exactlyZero = function ( $value ) { return $value === 0; };
			$needlePos = partialRight( 'strpos', $needle );

			return pipe( $needlePos, $exactlyZero );
		};

		$wrapper    = Wrapper::of( $value );

		$this->assertNotNull( $wrapper->filter( $startsWith( 'some' ) ) );
		$this->assertNotNull( $wrapper->map( 'strtoupper' )->filter( $startsWith( 'SOME' ) ) );
		$this->assertNull( $wrapper->filter( $startsWith( 'value' ) ) );
	}

	/**
	 * @test
	 */
	public function it_filters_using_identity_by_default() {

		$value = 'some value';

		$wrapper = Wrapper::of( $value );
		$this->assertEquals( $value, $wrapper->filter() );

	}

	/**
	 * @test
	 */
	public function of_is_curried() {
		$value = 'something';

		$of = Wrapper::of();
		$wrapper = $of( $value );

		$this->assertEquals( $value, $wrapper->get() );
	}


}
