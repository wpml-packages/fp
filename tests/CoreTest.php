<?php

use PHPUnit\Framework\TestCase;

use WPML\FP\Wrapper;
use WPML\FP\Right;
use WPML\FP\Left;
use \WPML\FP\FP;
use \WPML\Collect\Support\Collection;
use function WPML\FP\curry;
use function WPML\FP\curryN;
use function WPML\FP\flip;
use function WPML\FP\partial;
use function WPML\FP\partialRight;
use function WPML\FP\spreadArgs;
use function WPML\FP\gatherArgs;
use function WPML\FP\tap;
use function WPML\FP\apply;
use function WPML\FP\tryCatch;
use function WPML\FP\invoke;

class CoreTest extends TestCase {

	/**
	 * @test
	 */
	public function identity_returns_value() {
		$data = 'something';
		$this->assertEquals( $data, FP::identity( $data ) );
	}

	/**
	 * @test
	 */
	public function identity_used_as_filter_for_empty_values() {
		$filtered = wpml_collect( [ 'test', 'this', '', 'out' ] )->filter( FP::identity() )->values();
		$this->assertEquals( [ 'test', 'this', 'out' ], $filtered->toArray() );
	}

	/**
	 * @test
	 */
	public function it_spreads_arguments() {
		$arg1 = 'arg1';
		$arg2 = 'arg2';

		$testFunction = function ( $arg1, $arg2 ) {
			return $arg1 . $arg2;
		};

		$this->assertEquals(
			$testFunction( $arg1, $arg2 ),
			Wrapper::of( spreadArgs( $testFunction ) )->ap( [ $arg1, $arg2 ] )->get()
		);
	}

	/**
	 * @test
	 */
	public function it_can_use_spread_arguments_when_chaining() {
		$arg1 = 'arg1';
		$arg2 = 'arg2';

		$functionTakingTwoArgs = function ( $arg1, $arg2 ) {
			return $arg1 . $arg2;
		};

		$functionReturningTwoValues = function ( $arg1, $arg2 ) {
			return [ $arg1, $arg2 ];
		};

		$this->assertEquals(
			$functionTakingTwoArgs( $arg1, $arg2 ),
			Wrapper::of( spreadArgs( $functionTakingTwoArgs ) )->ap( $functionReturningTwoValues( $arg1, $arg2 ) )->get()
		);
	}

	/**
	 * @test
	 */
	public function it_gathers_arguments() {
		$arg1 = 'arg1';
		$arg2 = 'arg2';

		$testFunction = curryN( 2, function ( $arg ) {
			return $arg[0] . $arg[1];
		} );

		$this->assertEquals(
			$testFunction( [ $arg1, $arg2] ),
			Wrapper::of( gatherArgs( $testFunction ) )->ap( $arg1, $arg2 )->get()
		);
	}

	/**
	 * @test
	 */
	public function it_can_create_a_partial() {

		$add = function ( $a, $b ) {
			return $a + $b;
		};

		$add10 = partial( $add, 10 );

		$this->assertEquals( 11, $add10( 1 ) );
		$this->assertEquals( [ 11, 12, 13 ], array_map( $add10, [ 1, 2, 3 ] ) );
	}

	/**
	 * @test
	 */
	public function it_can_create_a_right_partial() {

		$sub = function ( $a, $b ) {
			return $a - $b;
		};

		$sub10 = partialRight( $sub, 10 );

		$this->assertEquals( 1, $sub10( 11 ) );
		$this->assertEquals( [ - 9, - 8, - 7 ], array_map( $sub10, [ 1, 2, 3 ] ) );

		// CAUTION: partialRight does not guarantee the right parameters. It only does when called with right number
		// for remaining parameters.

		$this->assertNotEquals( 1, $sub10( 11, 1 ) );
		$this->assertEquals( $sub10( 11, 1 ), 10 );
	}

	/**
	 * @test
	 */
	public function it_taps() {

		$data = Wrapper::of( 'something' );

		$checkTapIsCalledWithData = function ( $value ) use ( $data ) {
			$this->assertEquals( $data->get(), $value );

			return 'other';
		};

		$result = $data->map( tap( $checkTapIsCalledWithData ) );
		$this->assertEquals( $data->get(), $result->get() );

	}

	/**
	 * @test
	 */
	public function it_curries_2() {
		$add = function ( $x, $y ) {
			return $x + $y;
		};

		$addCurried = curry( $add );

		$add10 = $addCurried( 10 );
		$this->assertTrue( is_callable( $add10 ) );

		$this->assertEquals( 15, $add10( 5 ) );

		$this->assertEquals( 15, $addCurried( 10, 5 ) );
	}

	/**
	 * @test
	 */
	public function it_curries_3() {
		$add = function ( $x, $y, $z ) {
			return $x + $y + $z;
		};

		$this->assertEquals( 22, $add( 10, 5, 7 ) );

		$addCurried = curry( $add );

		$this->assertEquals( 22, $addCurried( 10, 5, 7 ) );

		$add10 = $addCurried( 10 );

		$this->assertEquals( 22, $add10( 5, 7 ) );

		$add15 = $add10( 5 );

		$this->assertEquals( 22, $add15( 7 ) );

	}

	/**
	 * @test
	 */
	public function it_curriesN() {
		$add = function ( $x, $y, $z ) {
			return $x + $y + $z;
		};

		$this->assertEquals( 22, $add( 10, 5, 7 ) );

		$addCurried = curryN( 3, $add );

		$this->assertEquals( 22, $addCurried( 10, 5, 7 ) );

		$add10 = $addCurried( 10 );

		$this->assertEquals( 22, $add10( 5, 7 ) );

		$add15 = $add10( 5 );

		$this->assertEquals( 22, $add15( 7 ) );

	}

	/**
	 * @test
	 */
	public function it_reduces() {

		$numbers = wpml_collect( [ 1, 2, 3, 4, 5, 8, 19 ] );
		$initial = 0;
		$reducer = FP::reduce( function ( $a, $b ) { return $a + $b; }, $initial );
		$this->assertEquals( 42, $reducer( $numbers ) );

		$initial = 10;
		$reducer = FP::reduce( function ( $a, $b ) { return $a + $b; }, $initial );
		$this->assertEquals( 52, $reducer( $numbers ) );
	}

	/**
	 * @test
	 */
	public function it_applies() {

		$numbers = wpml_collect( [ 1, 2, 3, 4, 5, 8, 19 ] );
		$initial = 0;
		$reducer = apply( 'reduce', function ( $a, $b ) { return $a + $b; }, $initial );
		$this->assertEquals( 42, $reducer( $numbers ) );

		$initial = 10;
		$reducer = apply( 'reduce', function ( $a, $b ) { return $a + $b; }, $initial );
		$this->assertEquals( 52, $reducer( $numbers ) );
	}

	/**
	 * @test
	 */
	public function it_returns_right_on_tryCatch_success() {
		$fn     = function () { return 2; };
		$result = tryCatch( $fn );
		$this->assertInstanceOf( Right::class, $result );
		$this->assertEquals( 2, $result->get() );
	}

	/**
	 * @test
	 */
	public function it_returns_left_on_tryCatch_error() {
		$fn     = function () { return 10 / 0; };
		$result = tryCatch( $fn );
		$this->assertInstanceOf( Left::class, $result );
		$result->orElse( function ( \Exception $e ) {
			$this->assertEquals( 'Division by zero', $e->getMessage() );
		} );
	}

	/**
	 * @test
	 */
	public function it_flips() {
		$concat = function ( $a, $b, $c ) {
			return $a . '-' . $b . '-' . $c;
		};

		$this->assertEquals( 'one-two-three', $concat( 'one', 'two', 'three' ) );

		$flipped = flip( $concat );
		$this->assertEquals( 'two-one-three', $flipped( 'one', 'two', 'three' ) );
	}

	/**
	 * @test
	 */
	public function it_invokes() {
		$str     = 'Multiple is: ';
		$invoker = invoke( 'multiply' )->with( 2, 8 );
		$this->assertEquals( 'Multiple is: 16', $invoker( new InvokerTestClass( $str ) ) );
	}

	/**
	 * @test
	 */
	public function test_always() {
		$value  = 123;
		$always = FP::always( $value );

		$this->assertEquals( $value, $always( 456 ) );

		$this->assertEquals( $value, FP::always( $value, 456 ) );
	}

	/**
	 * @test
	 */
	public function test_converge() {
		$divide = curryN( 2, function ( $num, $dom ) { return $num / $dom; } );
		$sum    = function ( Collection $collection ) { return $collection->sum(); };
		$length = function ( Collection $collection ) { return $collection->count(); };

		$average = FP::converge( $divide, [ $sum, $length ] );
		$this->assertEquals( 4, $average( wpml_collect( [ 1, 2, 3, 4, 5, 6, 7 ] ) ) );
	}


}

class InvokerTestClass {

	private $str;

	public function __construct( $str ) {
		$this->str = $str;
	}

	public function multiply( $x, $y ) {
		return $this->str . ( $x * $y );
	}
}
