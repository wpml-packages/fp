<?php

namespace WPML\FP\Invoker;

use PHPUnit\Framework\TestCase;
use WPML\FP\Fns;

class BeforeAfterTest extends TestCase {

	/**
	 * @test
	 */
	public function it_creates_with_of() {
		$this->assertInstanceOf( BeforeAfter::class, BeforeAfter::of( Fns::noop(), Fns::noop() ) );
	}

	/**
	 * @test
	 */
	public function it_runs_before_then_function_then_after() {
		$functionReturn = 'something';

		$log     = '';
		$before  = function () use ( &$log ) {
			$log .= 'before:';
		};
		$after   = function () use ( &$log ) {
			$log .= ':after';
		};
		$testFns = function ( $value1, $value2 ) use ( &$log, $functionReturn ) {
			$log .= $value1 . ':' . $value2;

			return $functionReturn;
		};

		$result = BeforeAfter::of( $before, $after )
		                     ->invoke( $testFns )
		                     ->runWith( 'test1', 'test2' );

		$this->assertEquals( $functionReturn, $result );

		$this->assertEquals( 'before:test1:test2:after', $log );
	}

	/**
	 * @test
	 */
	public function it_can_be_chained() {
		$functionReturn = 'something';

		$log     = '';
		$before1 = function () use ( &$log ) {
			$log .= 'before1:';
		};
		$after1  = function () use ( &$log ) {
			$log .= ':after1';
		};
		$before2 = function () use ( &$log ) {
			$log .= 'before2:';
		};
		$after2  = function () use ( &$log ) {
			$log .= ':after2';
		};
		$testFns = function ( $value1, $value2 ) use ( &$log, $functionReturn ) {
			$log .= $value1 . ':' . $value2;

			return $functionReturn;
		};

		$result = BeforeAfter::of( $before1, $after1 )
		                     ->then( $before2, $after2 )
		                     ->invoke( $testFns )
		                     ->runWith( 'test1', 'test2' );

		$this->assertEquals( $functionReturn, $result );

		$this->assertEquals( 'before1:before2:test1:test2:after2:after1', $log );
	}


}
