<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class MathTest extends TestCase {

	/**
	 * @test
	 */
	public function it_multiplies() {
		$this->assertEquals( 4, Math::multiply( 2, 2 ) );
		$this->assertNotEquals( 4, Math::multiply( 2, 3 ) );
	}

	/**
	 * @test
	 */
	public function it_divides() {
		$this->assertEquals( 4, Math::divide( 8, 2 ) );
		$this->assertEquals( 8, Math::divide( 24, 3 ) );
		$this->assertEquals( 6.5, Math::divide( 13, 2 ), '', 0.1 );
	}

	/**
	 * @test
	 */
	public function it_adds() {
		$this->assertEquals( 5, Math::add( 2, 3 ) );
		$this->assertNotEquals( 5, Math::add( 4, 3 ) );
	}

	/**
	 * @test
	 */
	public function test_product() {
		$this->assertEquals( 38400, Math::product( [ 2, 4, 6, 8, 100, 1 ] ) );
	}


}
