<?php

namespace WPML\FP;

class TestObject {

	public $property = 'property_value';

	public $property2;

	public function __get( $key ) {
		switch ( $key ) {
			case 'test1':
				return 'value1';
			case 'test2':
				return 'value2';
			case 'test3':
				return 5;
			case 'test4':
				return null;
			default:
				return null;
		}
	}

	public function __isset( $key ) {
		return Lst::includes( $key, [ 'test1', 'test2', 'test3', 'test4' ] );
	}

}