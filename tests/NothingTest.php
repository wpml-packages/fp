<?php

namespace WPML\FP;

use Exception;
use PHPUnit\Framework\TestCase;

class NothingTest extends TestCase {

	/**
	 * @test
	 */
	public function it_returns_same_for_map() {
		$nothing = new Nothing();
		$this->assertSame( $nothing, $nothing->map( 'strtoupper' ) );
	}

	/**
	 * @test
	 */
	public function it_throws_an_exception_on_get() {
		$this->expectException( Exception::class );
		$nothing = new Nothing();
		$nothing->get();
	}

	/**
	 * @test
	 */
	public function it_returns_other_on_getOrElse() {
		$nothing = new Nothing();

		$else = 'something else';
		$this->assertEquals( $else, $nothing->getOrElse( $else ) );
		$this->assertEquals( $else, $nothing->getOrElse( function () use ( $else ) { return $else; } ) );
	}

	/**
	 * @test
	 */
	public function it_returns_same_for_filter() {
		$nothing = new Nothing();
		$this->assertSame( $nothing, $nothing->filter( 'strtoupper' ) );
	}

	/**
	 * @test
	 */
	public function it_returns_same_for_chain() {
		$nothing = new Nothing();
		$this->assertSame( $nothing, $nothing->chain( 'strtoupper' ) );
	}

	/**
	 * @test
	 */
	public function it_returns_state() {
		$nothing = new Nothing();
		$this->assertTrue( $nothing->isNothing() );
		$this->assertFalse( $nothing->isJust() );
	}

	/**
	 * @test
	 */
	public function applicative_is_noop() {
		$string = 'test';

		$nothing = new Nothing();
		$this->assertTrue( $nothing->ap( Either::of( $string ) )->isNothing() );
	}

}
