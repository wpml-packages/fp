<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class CurryableTest extends TestCase {

	/**
	 * @test
	 *
	 */
	public function throws_an_exception_for_curry_instance_method() {
		$state        = 10;
		$testInstance = new TestCurryInstance( $state );

		$this->expectExceptionMessage( 'Curryable does not support methods in object scope. This is a limitation of PHP 5.x.' );

		$this->assertEquals( [ $state, 1, 2, 3 ], $testInstance->get( 1, 2, 3 ) );
		$this->assertEquals( [ $state, 1, 2, 3 ], $W( $testInstance->get() )->ap( 1 )->ap( 2 )->ap( 3 )->get() );
		$this->assertEquals( [ $state, 1, 2, 3 ], $W( $testInstance->get( 1, 2 ) )->ap( 3 )->get() );
		$this->assertEquals( [ $state, 1, 2, 3 ], $W( $testInstance->get( Fns::__, 2, 3 ) )->ap( 1 )->get() );

		$state2         = 20;
		$secondInstance = new TestCurryInstance( $state2 );
		$this->assertEquals( [ $state2, 1, 2, 3 ], $secondInstance->get( 1, 2, 3 ) );
		$this->assertEquals( [ $state2, 1, 2, 3 ], $W( $secondInstance->get() )->ap( 1 )->ap( 2 )->ap( 3 )->get() );
		$this->assertEquals( [ $state2, 1, 2, 3 ], $W( $secondInstance->get( 1, 2 ) )->ap( 3 )->get() );
		$this->assertEquals( [ $state2, 1, 2, 3 ], $W( $secondInstance->get( Fns::__, 2, 3 ) )->ap( 1 )->get() );
	}

	/**
	 * @test
	 */
	public function can_curry_static_method() {
		$state = 10;
		TestCurryStatic::init( $state );

		$W = [ Wrapper::class, 'of' ];

		$this->assertEquals( [ $state, 1, 2, 3 ], TestCurryStatic::get( 1, 2, 3 ) );
		$this->assertEquals( [ $state, 1, 2, 3 ], $W( TestCurryStatic::get( 1 ) )->ap( 2 )->ap( 3 )->get() );
		$this->assertEquals( [ $state, 1, 2, 3 ], $W( TestCurryStatic::get( 1, 2 ) )->ap( 3 )->get() );
		$this->assertEquals( [ $state, 1, 2, 3 ], $W( TestCurryStatic::get( Fns::__, 2, 3 ) )->ap( 1 )->get() );
	}

}

/**
 * Class TestCurryInstance
 * @package WPML\FP
 * @method callable|array get( ...$a, ...$b, ...$c )
 */
class TestCurryInstance {

	use Curryable;

	private $state;

	public function __construct( $state ) {
		$this->state = $state;
	}
}

TestCurryInstance::curryN( 'get', 3, function ( $a, $b, $c ) {
	return [ $this->state, $a, $b, $c ];
} );

/**
 * Class TestCurryStatic
 * @package WPML\FP
 * @method callable|array get( ...$a, ...$b, ...$c )
 */
class TestCurryStatic {

	use Curryable;

	private static $state;

	public static function init( $state ) {
		self::$state = $state;
	}
}

TestCurryStatic::curryN( 'get', 3, function ( $a, $b, $c ) {
	return [ self::$state, $a, $b, $c ];
} );

