<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;
use tad\FunctionMocker\FunctionMocker;

class LstTest extends TestCase {

	/**
	 * @test
	 */
	public function test_append() {
		$arr    = [ 'a', 'b', 'c' ];
		$newArr = Lst::append( 'd', $arr );

		$this->assertCount( 3, $arr );
		$this->assertCount( 4, $newArr );
		$this->assertEquals( 'd', $newArr[3] );

		$newArr = Lst::append( [ 'd' ], $arr );
		$this->assertCount( 4, $newArr );
		$this->assertEquals( [ 'd' ], $newArr[3] );

		$newArr = Lst::append( 'd', [] );
		$this->assertCount( 1, $newArr );
		$this->assertEquals( 'd', $newArr[0] );
	}

	/**
	 * @test
	 */
	public function test_fromPairs() {
		$this->assertEquals(
			[
				'a' => 1,
				'b' => 2,
				'c' => 3,
			],
			Lst::fromPairs( [
					[ 'a', 1 ],
					[ 'b', 2 ],
					[ 'c', 3 ],
				]
			)
		);
	}

	/**
	 * @test
	 */
	public function it_casts_to_object() {
		$data = [
			'a' => 1,
			'b' => 2,
			'c' => 3,
		];

		$this->assertEquals( (object) $data, Lst::toObj( $data ) );
	}

	/**
	 * @test
	 */
	public function it_can_pluck() {
		$products = [
			[ 'name' => 'Jeans', 'price' => 80, 'category' => 'clothes' ],
			(object) [ 'name' => 'Hoodie', 'price' => 60, 'category' => 'clothes' ],
			wpml_collect( [ 'name' => 'Jacket', 'price' => 120, 'category' => 'clothes' ] ),
			[ 'name' => 'Cards', 'price' => 35, 'category' => 'games' ],
		];

		$this->assertEquals( [ 'Jeans', 'Hoodie', 'Jacket', 'Cards' ], Lst::pluck( 'name', $products ) );
		$this->assertEquals( [ 'clothes', 'clothes', 'clothes', 'games' ], Lst::pluck( 'category', $products ) );
	}

	/**
	 * @test
	 */
	public function it_partitions_array() {
		$isEven = function ( $n ) {
			return $n % 2 === 0;
		};
		$this->assertEquals( [ [ 2, 4 ], [ 1, 3 ] ], Lst::partition( $isEven, [ 1, 2, 3, 4 ] ) );
	}

	/**
	 * @test
	 */
	public function it_partitions_object_with_filter_method() {
		$isEven = function ( $n ) {
			return $n % 2 === 0;
		};

		list( $even, $odd ) = Lst::partition( $isEven, wpml_collect( [ 1, 2, 3, 4 ] ) );
		$this->assertEquals( wpml_collect( [ 2, 4 ] ), $even->values() );
		$this->assertEquals( wpml_collect( [ 1, 3 ] ), $odd->values() );
	}

	/**
	 * @test
	 */
	public function it_sorts() {
		$diff = function ( $a, $b ) {
			return $a - $b;
		};
		$gt = function ( $a, $b ) {
			return $a > $b;
		};

		$original = [ 4, 2, 7, 5 ];
		$this->assertEquals( [ 2, 4, 5, 7 ], Lst::sort( $diff, $original ) );
		$this->assertEquals( [ 4, 2, 7, 5 ], $original, 'original is not mutated.' );

		$original = wpml_collect( [ 4, 2, 7, 5 ] );
		$this->assertEquals( wpml_collect( [ 2, 4, 5, 7 ] ), Lst::sort( $diff, $original ) );

		// Sorts a boolean function
		$original = wpml_collect( [ 4, 2, 7, 5, 3, 9 ] );
		$this->assertEquals( wpml_collect( [ 2, 3, 4, 5, 7, 9 ] ), Lst::sort( $gt, $original ) );

	}

	/**
	 * @test
	 */
	public function it_unfolds() {
		$f = function ( $n ) {
			return $n > 50 ? false : [ - $n, $n + 10 ];
		};
		$this->assertEquals( [ - 10, - 20, - 30, - 40, - 50 ], Lst::unfold( $f, 10 ) );
	}

	/**
	 * @test
	 */
	public function it_zips() {
		$this->assertEquals( [ [ 1, 'a' ], [ 2, 'b' ], [ 3, 'c' ] ], Lst::zip( [ 1, 2, 3 ], [ 'a', 'b', 'c' ] ) );
		$this->assertEquals( [ [ 1, 'a' ], [ 2, 'b' ], [ 3, 'c' ] ], Lst::zip( [ 1, 2, 3, 4 ], [ 'a', 'b', 'c' ] ) );
		$this->assertEquals( [ [ 1, 'a' ], [ 2, 'b' ], [ 3, 'c' ] ], Lst::zip( [ 1, 2, 3 ], [ 'a', 'b', 'c', 'd' ] ) );
	}

	/**
	 * @test
	 */
	public function it_zips_to_assoc_array() {
		$this->assertEquals( [ 'a' => 1, 'b' => 2, 'c' => 3 ], Lst::zipObj( [ 'a', 'b', 'c' ], [ 1, 2, 3 ] ) );
		$this->assertEquals( [ 'a' => 1, 'b' => 2, 'c' => 3 ], Lst::zipObj( [ 'a', 'b', 'c' ], [ 1, 2, 3, 4 ] ) );
		$this->assertEquals( [ 'a' => 1, 'b' => 2, 'c' => 3 ], Lst::zipObj( [ 'a', 'b', 'c', 'd' ], [ 1, 2, 3 ] ) );
	}


	/**
	 * @test
	 */
	public function it_zips_with_function() {
		$f = function ( $a, $b ) {
			return "f({$a},{$b})";
		}; // f(a,b)

		$this->assertEquals( [ 'f(1,a)', 'f(2,b)', 'f(3,c)' ], Lst::zipWith( $f, [ 1, 2, 3 ], [ 'a', 'b', 'c' ] ) );
		$this->assertEquals( [ 'f(1,a)', 'f(2,b)', 'f(3,c)' ], Lst::zipWith( $f, [ 1, 2, 3, 4 ], [ 'a', 'b', 'c' ] ) );
		$this->assertEquals(
			[ 'f(1,a)', 'f(2,b)', 'f(3,c)' ],
			Lst::zipWith( $f, [ 1, 2, 3 ], [ 'a', 'b', 'c', 'd' ] )
		);
	}

	/**
	 * @test
	 */
	public function it_joins() {
		$spacer = Lst::join( ' ' );
		$this->assertEquals( 'a 2 3.4', $spacer( [ 'a', 2, 3.4 ] ) );
		$this->assertEquals( '1|2|3', Lst::join( '|', [ 1, 2, 3 ] ) );
	}

	/**
	 * @test
	 */
	public function it_joins_with_commas_and_and() {
		FunctionMocker::replace( '__', Fns::identity() );
		$this->assertEquals( '', Lst::joinWithCommasAndAnd( [] ) );
		$this->assertEquals( 'one', Lst::joinWithCommasAndAnd( [ 'one' ] ) );
		$this->assertEquals( 'one and two', Lst::joinWithCommasAndAnd( [ 'one', 'two' ] ) );
		$this->assertEquals( 'one, two and three', Lst::joinWithCommasAndAnd( [ 'one', 'two', 'three' ] ) );
	}

	/**
	 * @test
	 */
	public function it_concats() {
		$this->assertEquals( [ 4, 5, 6, 1, 2, 3 ], Lst::concat( [ 4, 5, 6 ], [ 1, 2, 3 ] ) );
		$this->assertEquals( [], Lst::concat( [], [] ) );
	}

	/**
	 * @test
	 */
	public function it_finds() {
		$xs   = [ [ 'a' => 1, 'b' => 1 ], [ 'a' => 2, 'b' => 2 ], [ 'a' => 3, 'b' => 3 ] ];
		$aeq2 = Lst::find( Relation::propEq( 'a', 2 ) );
		$this->assertEquals( [ 'a' => 2, 'b' => 2 ], $aeq2( $xs ) );
		$beq3 = Lst::find( Relation::propEq( 'b', 3 ) );
		$this->assertEquals( [ 'a' => 3, 'b' => 3 ], $beq3( $xs ) );
		$aeq4 = Lst::find( Relation::propEq( 'a', 4 ) );
		$this->assertNull( $aeq4( $xs ) );
	}

	/**
	 * @test
	 */
	public function it_flattens() {
		$array = [ 1, 2, [ 3, 4 ], 5, [ 6, [ 7, 8, [ 9, [ 10, 11 ], 12 ] ] ] ];
		$this->assertEquals(
			[ 1, 2, 3, 4, 5, 6, [ 7, 8, [ 9, [ 10, 11 ], 12 ] ] ],
			Lst::flattenToDepth( 1, $array )
		);

		$flatterToDepthTwo = Lst::flattenToDepth( 2 );
		$this->assertEquals(
			[ 1, 2, 3, 4, 5, 6, 7, 8, [ 9, [ 10, 11 ], 12 ] ],
			$flatterToDepthTwo( $array )
		);

		$this->assertEquals(
			[ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ],
			Lst::flatten( $array )
		);
	}

	/**
	 * @test
	 */
	public function test_includes() {
		$includes10 = Lst::includes( 10 );

		$this->assertTrue( $includes10( [ 5, 10, 15, 20 ] ) );
		$this->assertFalse( $includes10( [ 5, 15, 20 ] ) );
	}

	/**
	 * @test
	 */
	public function test_includesAll() {
		$includes10and20 = Lst::includesAll( [ 10, 20 ] );

		$this->assertTrue( $includes10and20( [ 5, 10, 15, 20 ] ) );
		$this->assertFalse( $includes10and20( [ 5, 15, 20 ] ) );
	}

	/**
	 * @test
	 */
	public function test_nth() {
		$this->assertEquals( 1, Lst::nth( 0, [ 1, 2, 3, 4 ] ) );
		$this->assertEquals( 2, Lst::nth( 1, [ 1, 2, 3, 4 ] ) );
		$this->assertEquals( 3, Lst::nth( 2, [ 1, 2, 3, 4 ] ) );
		$this->assertEquals( 4, Lst::nth( 3, [ 1, 2, 3, 4 ] ) );

		$this->assertNull( Lst::nth( 4, [ 1, 2, 3, 4 ] ) );

		$this->assertEquals( 4, Lst::nth( - 1, [ 1, 2, 3, 4 ] ) );
		$this->assertEquals( 3, Lst::nth( - 2, [ 1, 2, 3, 4 ] ) );
		$this->assertEquals( 2, Lst::nth( - 3, [ 1, 2, 3, 4 ] ) );
		$this->assertEquals( 1, Lst::nth( - 4, [ 1, 2, 3, 4 ] ) );

		$this->assertNull( Lst::nth( - 5, [ 1, 2, 3, 4 ] ) );
	}

	/**
	 * @test
	 */
	public function test_first() {
		$this->assertEquals( 1, Lst::first( [ 1, 2, 3, 4 ] ) );
		$this->assertNull( Lst::first( [] ) );
	}

	/**
	 * @test
	 */
	public function test_last() {
		$this->assertEquals( 4, Lst::last( [ 1, 2, 3, 4 ] ) );
		$this->assertNull( Lst::last( [] ) );
	}

	/**
	 * @test
	 */
	public function test_length() {
		$this->assertEquals( 0, Lst::length( [] ) );
		$this->assertEquals( 3, Lst::length( [ 'fi', 'fo', 'fum' ] ) );
	}

	/**
	 * @test
	 */
	public function test_take() {
		$this->assertEquals( [ 'fi' ], Lst::take( 1, [ 'fi', 'fo', 'fum' ] ) );
		$this->assertEquals( [ 'fi', 'fo' ], Lst::take( 2, [ 'fi', 'fo', 'fum' ] ) );
	}

	/**
	 * @test
	 */
	public function test_takeLast() {
		$this->assertEquals( [ 'fum' ], Lst::takeLast( 1, [ 'fi', 'fo', 'fum' ] ) );
		$this->assertEquals( [ 'fo', 'fum' ], Lst::takeLast( 2, [ 'fi', 'fo', 'fum' ] ) );
	}

	/**
	 * @test
	 */
	public function test_slice() {
		$arr = [ 'a', 'b', 'c', 'd', 'e' ];

		$this->assertEquals( $arr, Lst::slice( 0, null, $arr ) );
		$this->assertEquals( [ 'b', 'c', 'd', 'e' ], Lst::slice( 1, null, $arr ) );
		$this->assertEquals( [ 'b', 'c' ], Lst::slice( 1, 2, $arr ) );

		$this->assertEquals( [ 'd', 'e' ], Lst::slice( - 2, 2, $arr ) );
	}

	/**
	 * @test
	 */
	public function test_drop() {
		$this->assertEquals( [ 'fum' ], Lst::drop( 2, [ 'fi', 'fo', 'fum' ] ) );
		$this->assertEquals( [ 'fo', 'fum' ], Lst::drop( 1, [ 'fi', 'fo', 'fum' ] ) );
	}

	/**
	 * @test
	 */
	public function test_dropLast() {
		$this->assertEquals( [ 'fi' ], Lst::dropLast( 2, [ 'fi', 'fo', 'fum' ] ) );
		$this->assertEquals( [ 'fi', 'fo' ], Lst::dropLast( 1, [ 'fi', 'fo', 'fum' ] ) );
	}

	/**
	 * @test
	 */
	public function test_makePair() {
		$this->assertEquals( [ 'a', 'b' ], Lst::makePair( 'a', 'b' ) );
	}

	/**
	 * @test
	 */
	public function test_make() {
		$this->assertEquals( [ 'a' ], Lst::make( 'a' ) );
		$this->assertEquals( [ 'a', 1, 'c' ], Lst::make( 'a', 1, 'c' ) );
	}

	/**
	 * @test
	 */
	public function test_insert() {
		$array = [ 1, 2, 3, 4 ];

		$this->assertEquals( [ 1, 2, 'x', 3, 4 ], Lst::insert( 2, 'x', $array ) );
		$this->assertEquals( [ 'x', 1, 2, 3, 4 ], Lst::insert( 0, 'x', $array ) );
		$this->assertEquals( [ 1, 2, 3, 4, 'x' ], Lst::insert( 4, 'x', $array ) );

		$this->assertEquals( [ 1, 2, [ 'x', 'y' ], 3, 4 ], Lst::insert( 2, [ 'x', 'y' ], $array ) );

	}

	/**
	 * @test
	 */
	public function test_range() {
		$this->assertEquals( [ 1, 2, 3, 4, 5 ], Lst::range( 1, 5 ) );
		$this->assertEquals( [ 50, 51, 52, 53 ], Lst::range( 50, 53 ) );
	}

	/**
	 * @test
	 */
	public function test_xprod() {
		$a              = [ 1, 2, 3 ];
		$b              = [ 'a', 'b', 'c' ];
		$expectedResult = [
			[ 1, 'a' ],
			[ 1, 'b' ],
			[ 1, 'c' ],
			[ 2, 'a' ],
			[ 2, 'b' ],
			[ 2, 'c' ],
			[ 3, 'a' ],
			[ 3, 'b' ],
			[ 3, 'c' ],
		];
		$this->assertEquals( $expectedResult, Lst::xprod( $a, $b ) );
	}

	/**
	 * @test
	 */
	public function it_prepends() {
		$this->assertEquals( [ 'fee', 'fi', 'fo', 'fum' ], Lst::prepend( 'fee', [ 'fi', 'fo', 'fum' ] ) );
	}

	/**
	 * @test
	 */
	public function it_reverses() {
		$numbers = [ 1, 2, 3, 4, 5, 8, 19 ];
		$this->assertEquals( array_reverse( $numbers ), Lst::reverse( $numbers ) );
	}

	/**
	 * @test
	 */
	public function it_keys_by_field() {
		$data = [
			[ 'x' => 'a', 'y' => 123 ],
			[ 'x' => 'b', 'y' => 456 ],
		];

		$this->assertEquals(
			[
				'a' => [ 'x' => 'a', 'y' => 123 ],
				'b' => [ 'x' => 'b', 'y' => 456 ],
			],
			Lst::keyBy( 'x', $data )
		);

	}

	/**
	 * @test
	 */
	public function test_key_with() {
		$data     = [ 1, 2.3, 'some data', - 2, 'a' ];
		$expected = [
			[ 'myKey' => 1 ],
			[ 'myKey' => 2.3 ],
			[ 'myKey' => 'some data' ],
			[ 'myKey' => - 2 ],
			[ 'myKey' => 'a' ]
		];

		$this->assertEquals( $expected, Lst::keyWith( 'myKey', $data ) );
	}

	/**
	 * @test
	 */
	public function it_returns_diff() {
		$original  = [ 1, 2, 3, 4, 5, 6 ];
		$compareTo = [ 2, 4, 6, 8 ];
		$expected  = [ 1, 3, 5 ];

		$this->assertEquals( wpml_collect( $expected ), Lst::diff( wpml_collect( $original ), $compareTo )->values() );

		$this->assertEquals( $expected, array_values( Lst::diff( $original, $compareTo ) ) );

	}

	/**
	 * @test
	 */
	public function it_repeats_value() {
		$times = 3;
		$val   = 12;
		$this->assertEquals( [ $val, $val, $val ], Lst::repeat( $val, $times ) );

		$val = 'some string';
		$this->assertEquals( [ $val, $val, $val ], Lst::repeat( $val, $times ) );

		$val = [ 1, 'aaa', 23.4 ];
		$this->assertEquals( [ $val, $val, $val ], Lst::repeat( $val, $times ) );

		$val = (object) [ 'prop1' => 'val1', 'prop2' => 12 ];
		$this->assertEquals( [ $val, $val, $val ], Lst::repeat( $val, $times ) );
	}

	/**
	 * @test
	 */
	public function it_sums_array_or_collection() {
		$array = [ 2, 5, 8, 9 ];

		$this->assertEquals( 24, Lst::sum( $array ) );
		$this->assertEquals( 0, Lst::sum( [] ) );

		$this->assertEquals( 24, Lst::sum( wpml_collect( $array ) ) );
		$this->assertEquals( 0, Lst::sum( wpml_collect( [] ) ) );
	}
}
