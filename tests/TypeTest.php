<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

/**
 * @group type
 */
class TypeTest extends TestCase {

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsNull() {
		$this->assertTrue( Type::isNull( null ) );

		$this->assertFalse( Type::isNull( '' ) );
		$this->assertFalse( Type::isNull( 0 ) );
		$this->assertFalse( Type::isNull( [] ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsBool() {
		$this->assertTrue( Type::isBool( true ) );
		$this->assertTrue( Type::isBool( false ) );

		$this->assertFalse( Type::isBool( 0 ) );
		$this->assertFalse( Type::isBool( null ) );
		$this->assertFalse( Type::isBool( '' ) );
		$this->assertFalse( Type::isBool( [] ) );
		$this->assertFalse( Type::isBool( 'something' ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsInt() {
		$this->assertTrue( Type::isInt( 0 ) );
		$this->assertTrue( Type::isInt( 123 ) );

		$this->assertFalse( Type::isInt( '0' ) );
		$this->assertFalse( Type::isInt( '123' ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsNumeric() {
		$this->assertTrue( Type::isNumeric( 0 ) );
		$this->assertTrue( Type::isNumeric( 123 ) );
		$this->assertTrue( Type::isNumeric( 123.23 ) );
		$this->assertTrue( Type::isNumeric( '123' ) );

		$this->assertFalse( Type::isNumeric( 'a123' ) );
		$this->assertFalse( Type::isNumeric( '123a' ) );
		$this->assertFalse( Type::isNumeric( '123,23' ) );
		$this->assertFalse( Type::isNumeric( null ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsFloat() {
		$this->assertTrue( Type::isFloat( 123.23 ) );

		$this->assertFalse( Type::isFloat( 123 ) );
		$this->assertFalse( Type::isFloat( '123,23' ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsString() {
		$this->assertTrue( Type::isString( '' ) );
		$this->assertTrue( Type::isString( 'something' ) );

		$this->assertFalse( Type::isString( 123 ) );
		$this->assertFalse( Type::isString( null ) );
		$this->assertFalse( Type::isString( [] ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsScalar() {
		$this->assertTrue( Type::isScalar( '' ) );
		$this->assertTrue( Type::isScalar( 'something' ) );
		$this->assertTrue( Type::isScalar( 123 ) );
		$this->assertTrue( Type::isScalar( 123.23 ) );
		$this->assertTrue( Type::isScalar( true ) );
		$this->assertTrue( Type::isScalar( false ) );

		$this->assertFalse( Type::isScalar( null ) );
		$this->assertFalse( Type::isScalar( [] ) );
		$this->assertFalse( Type::isScalar( new \stdClass() ) );
		$this->assertFalse( Type::isScalar( function() {} ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsArray() {
		$this->assertTrue( Type::isArray( [] ) );
		$this->assertTrue( Type::isArray( [ 1, 2, 3 ] ) );
		$this->assertTrue( Type::isArray( [ 'foo' => 'bar' ] ) );

		$this->assertFalse( Type::isArray( 123 ) );
		$this->assertFalse( Type::isArray( 'something' ) );
		$this->assertFalse( Type::isArray( new \stdClass() ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsObject() {
		$this->assertTrue( Type::isObject( (object) [] ) );
		$this->assertTrue( Type::isObject( (object) [ 1, 2, 3 ] ) );
		$this->assertTrue( Type::isObject( (object) [ 'foo' => 'bar' ] ) );
		$this->assertTrue( Type::isObject( new \stdClass() ) );
		$this->assertTrue( Type::isObject( new class() {} ) );

		$this->assertFalse( Type::isObject( 123 ) );
		$this->assertFalse( Type::isObject( 'something' ) );
		$this->assertFalse( Type::isObject( [] ) );
		$this->assertFalse( Type::isObject( [ 'foo' => 'bar' ] ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsCallable() {
		$this->assertTrue( Type::isCallable( function() {} ) );
		$this->assertTrue( Type::isCallable( 'is_callable' ) );
		$this->assertTrue( Type::isCallable( [ $this, 'testIsCallable' ] ) );
		$this->assertTrue( Type::isCallable( new class() { function __invoke() {} } ) );

		$this->assertFalse( Type::isCallable( new \stdClass() ) );
		$this->assertFalse( Type::isCallable( 123 ) );
		$this->assertFalse( Type::isCallable( 'something' ) );
		$this->assertFalse( Type::isCallable( [ 'foo' => 'bar' ] ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsSerialized() {
		$this->assertTrue( Type::isSerialized( serialize( 123 ) ) );
		$this->assertTrue( Type::isSerialized( serialize( 123.23 ) ) );
		$this->assertTrue( Type::isSerialized( serialize( '123' ) ) );
		$this->assertTrue( Type::isSerialized( serialize( true ) ) );
		$this->assertTrue( Type::isSerialized( serialize( false ) ) );
		$this->assertTrue( Type::isSerialized( serialize( null ) ) );
		$this->assertTrue( Type::isSerialized( serialize( [ 123, 456 ] ) ) );
		$this->assertTrue( Type::isSerialized( serialize( [ '123', '456' ] ) ) );
		$this->assertTrue( Type::isSerialized( serialize( [ 'foo' => 'bar' ] ) ) );
		$this->assertTrue( Type::isSerialized( serialize( (object) [ 'foo' => 'bar' ] ) ) );
		$this->assertTrue( Type::isSerialized( serialize( new \stdClass() ) ) );

		$this->assertFalse( Type::isSerialized( 123 ) );
		$this->assertFalse( Type::isSerialized( 123.23 ) );
		$this->assertFalse( Type::isSerialized( 'something' ) );
		$this->assertFalse( Type::isSerialized( json_encode( 'something' ) ) );
		$this->assertFalse( Type::isSerialized( base64_encode( 'something' ) ) );
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsJson() {
		$this->assertTrue( Type::isJson( json_encode( [ 123, 456 ] ) ) );
		$this->assertTrue( Type::isJson( json_encode( [ '123', '456' ] ) ) );
		$this->assertTrue( Type::isJson( json_encode( [ 'foo' => 'bar' ] ) ) );
		$this->assertTrue( Type::isJson( json_encode( (object) [ 'foo' => 'bar' ] ) ) );
		$this->assertTrue( Type::isJson( json_encode( new \stdClass() ) ) );

		$this->assertFalse( Type::isJson( json_encode( 123 ) ) );
		$this->assertFalse( Type::isJson( json_encode( 123.23 ) ) );
		$this->assertFalse( Type::isJson( json_encode( '123' ) ) );
		$this->assertFalse( Type::isJson( json_encode( true ) ) );
		$this->assertFalse( Type::isJson( json_encode( false ) ) );
		$this->assertFalse( Type::isJson( json_encode( null ) ) );
		$this->assertFalse( Type::isJson( 123 ) );
		$this->assertFalse( Type::isJson( 123.23 ) );
		$this->assertFalse( Type::isJson( 'something' ) );
		$this->assertFalse( Type::isJson( serialize( 'something' ) ) );
		$this->assertFalse( Type::isJson( base64_encode( 'something' ) ) );
	}
}
