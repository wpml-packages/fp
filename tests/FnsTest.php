<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

use tad\FunctionMocker\FunctionMocker;
use \WPML\Collect\Support\Collection;

class FnsTest extends TestCase {

	/**
	 * @test
	 */
	public function identity_returns_value() {
		$data = 'something';
		$this->assertEquals( $data, Fns::identity( $data ) );
	}

	/**
	 * @test
	 */
	public function identity_used_as_filter_for_empty_values() {
		$filtered = wpml_collect( [ 'test', 'this', '', 'out' ] )->filter( Fns::identity() )->values();
		$this->assertEquals( [ 'test', 'this', 'out' ], $filtered->toArray() );
	}

	/**
	 * @test
	 */
	public function it_reduces() {

		$add     = function ( $a, $b ) { return $a + $b; };
		$numbers = wpml_collect( [ 1, 2, 3, 4, 5, 8, 19 ] );

		$initial = 0;
		$reducer = Fns::reduce( $add, $initial );
		$this->assertEquals( 42, $reducer( $numbers ) );

		$initial = 10;
		$reducer = Fns::reduce( $add, $initial );
		$this->assertEquals( 52, $reducer( $numbers ) );
	}

	/**
	 * @test
	 */
	public function it_reduces_right() {

		$numbers = [ 1, 2, 3, 4, 5, 8, 19 ];

		$reducer = Fns::reduceRight( flip( Lst::append() ), [] );

		$this->assertEquals( array_reverse( $numbers ), $reducer( $numbers ) );
		$this->assertEquals( array_reverse( $numbers ), $reducer( wpml_collect( $numbers ) ) );
	}

	/**
	 * @test
	 */
	public function test_always() {
		$value  = 123;
		$always = Fns::always( $value );
		$this->assertTrue( is_callable( $always ) );

		$this->assertEquals( $value, $always( 456 ) );
		$this->assertEquals( $value, $always() );
	}

	/**
	 * @test
	 */
	public function test_converge() {
		$divide = curryN( 2, function ( $num, $dom ) { return $num / $dom; } );
		$sum    = function ( Collection $collection ) { return $collection->sum(); };
		$length = function ( Collection $collection ) { return $collection->count(); };

		$average = Fns::converge( $divide, [ $sum, $length ] );
		$this->assertEquals( 4, $average( wpml_collect( [ 1, 2, 3, 4, 5, 6, 7 ] ) ) );
	}

	/**
	 * @test
	 */
	public function it_uses_placeholder_for_curried_function() {
		$f = curryN( 3, function ( $a, $b, $c ) {
			return [ $a, $b, $c ];
		} );

		$expected = [ 1, 2, 3 ];

		$_ = Fns::__;

		$W = [ Wrapper::class, 'of' ];
		$this->assertEquals( $expected, $f( 1, 2, 3 ) );
		$this->assertEquals( $expected, $W( $f( $_, 2, 3 ) )->ap( 1 )->get() ); //$W( $f( $_, 2, 3 ) )->ap( 1 )->get() same as $f( $_, 2, 3 )( 1 ) in PHP 7
		$this->assertEquals( $expected, $W( $f( $_, $_, 3 ) )->ap( 1 )->ap( 2 )->get() );
		$this->assertEquals( $expected, $W( $f( $_, $_, 3 ) )->ap( 1 )->ap( 2 )->get() );
		$this->assertEquals( $expected, $W( $f( $_, 2, $_ ) )->ap( 1 )->ap( 3 )->get() );
		$this->assertEquals( $expected, $W( $f( $_, 2 ) )->ap( 1 )->ap( 3 )->get() );
		$this->assertEquals( $expected, $W( $f( $_, 2 ) )->ap( 1 )->ap( 3 )->get() );


		$partial1 = $f( $_, 2 );
		$partial2 = $partial1( $_, 3 );
		$this->assertEquals( $expected, $partial2( 1 ) );
	}

	/**
	 * @test
	 */
	public function it_filters_array() {
		$isEven = function ( $n ) { return $n % 2 === 0; };
		$this->assertEquals( [ 2, 4 ], Fns::filter( $isEven, [ 1, 2, 3, 4 ] ) );
	}

	/**
	 * @test
	 */
	public function it_filters_with_filter_method() {
		$isEven = function ( $n ) { return $n % 2 === 0; };
		$this->assertEquals( wpml_collect( [ 2, 4 ] ), Fns::filter( $isEven, wpml_collect( [
			1,
			2,
			3,
			4
		] ) )->values() );
	}

	/**
	 * @test
	 */
	public function it_rejects_array() {
		$isEven = function ( $n ) { return $n % 2 === 0; };
		$this->assertEquals( [ 1, 3 ], Fns::reject( $isEven, [ 1, 2, 3, 4 ] ) );
	}

	/**
	 * @test
	 */
	public function it_rejects_with_reject_method() {
		$isEven = function ( $n ) { return $n % 2 === 0; };
		$this->assertEquals( wpml_collect( [ 1, 3 ] ), Fns::reject( $isEven, wpml_collect( [
			1,
			2,
			3,
			4
		] ) )->values() );
	}

	/**
	 * @test
	 */
	public function test_value_returns_its_value() {
		$data = 123;
		$this->assertEquals( $data, Fns::value( $data ) );
	}

	/**
	 * @test
	 */
	public function test_value_returns_value_from_callable() {
		$data = 123;
		$fn   = function () use ( $data ) { return $data; };
		$this->assertEquals( $data, Fns::value( $fn ) );
	}

	/**
	 * @test
	 */
	public function test_constructN() {

		$construct = Fns::constructN( 2, '\WPML\FP\constructNTestClass' );
		$instance  = Wrapper::of( $construct( 'abc' ) )->ap( 123 )->get();

		$this->assertInstanceOf( constructNTestClass::class, $instance );

		$this->assertEquals( [ 'abc', 123 ], $instance->get() );

		$pipeline = pipe(
			Fns::constructN( 2, '\WPML\FP\constructNTestClass' ),
			invoke( 'get' )
		);

		$this->assertEquals( [ 'abc', 123 ], $pipeline( 'abc', 123 ) );
	}

	/**
	 * @test
	 */
	public function test_ascend() {
		$byAge = Fns::ascend( Obj::prop( 'age' ) );

		$people = [
			[ 'name' => 'Emma', 'age' => 70 ],
			[ 'name' => 'Peter', 'age' => 78 ],
			[ 'name' => 'Mikhail', 'age' => 62 ],
		];

		$peopleByYoungestFirst = Lst::sort( $byAge, $people );
		$this->assertEquals( [
			[ 'name' => 'Mikhail', 'age' => 62 ],
			[ 'name' => 'Emma', 'age' => 70 ],
			[ 'name' => 'Peter', 'age' => 78 ],
		], $peopleByYoungestFirst );
	}

	/**
	 * @test
	 */
	public function test_descend() {
		$byAge = Fns::descend( Obj::prop( 'age' ) );

		$people = [
			[ 'name' => 'Emma', 'age' => 70 ],
			[ 'name' => 'Peter', 'age' => 78 ],
			[ 'name' => 'Mikhail', 'age' => 62 ],
		];

		$peopleByYoungestFirst = Lst::sort( $byAge, $people );
		$this->assertEquals( [
			[ 'name' => 'Peter', 'age' => 78 ],
			[ 'name' => 'Emma', 'age' => 70 ],
			[ 'name' => 'Mikhail', 'age' => 62 ],
		], $peopleByYoungestFirst );
	}

	/**
	 * @test
	 */
	public function test_useWith() {
		$countries = [
			[ 'cc' => 'GB', 'flag' => 'gb🇬🇧' ],
			[ 'cc' => 'US', 'flag' => 'us🇬🇧' ],
			[ 'cc' => 'CA', 'flag' => 'ca🇬🇧' ],
			[ 'cc' => 'FR', 'flag' => 'fr🇬🇧' ],
		];

		$getCountry = function ( $cc, $list ) { return Lst::find( Relation::propEq( 'cc', $cc ), $list ); };

		$this->assertEquals(
			[ 'cc' => 'FR', 'flag' => 'fr🇬🇧' ],
			$getCountry( 'FR', $countries )
		);

		$getCountryPointFree = Fns::useWith( Lst::find(), [ Relation::propEq( 'cc' ), Fns::identity() ] );
		$this->assertEquals(
			[ 'cc' => 'FR', 'flag' => 'fr🇬🇧' ],
			$getCountryPointFree( 'FR', $countries )
		);
	}

	/**
	 * @test
	 */
	public function it_gets_nth_arg() {
		$secondArg = Fns::nthArg( 1 );
		$this->assertEquals( 2, $secondArg( 1, 2, 3, 4 ) );

		$lastArg = Fns::nthArg( - 1 );
		$this->assertEquals( 4, $lastArg( 1, 2, 3, 4 ) );
	}

	/**
	 * @test
	 */
	public function test_either() {
		$right = Either::right( 'right wing' );
		$this->assertEquals( 'RIGHT WING', Fns::either( 'strrev', 'strtoupper', $right ) );

		$left = Either::left( 'left wing' );
		$this->assertEquals( 'gniw tfel', Fns::either( 'strrev', 'strtoupper', $left ) );
	}

	/**
	 * @test
	 */
	public function test_maybe() {
		$maybe = Maybe::fromNullable( 'Maybe' );
		$this->assertEquals( 'MAYBE', Fns::maybe( 'Not found', 'strtoupper', $maybe ) );

		$maybe = Maybe::fromNullable( null );
		$this->assertEquals( 'Not found', Fns::maybe( 'Not found', 'strtoupper', $maybe ) );
	}

	/**
	 * @test
	 */
	public function test_isRight() {
		$this->assertFalse( Fns::isRight( 123 ) );
		$this->assertTrue( Fns::isRight( Either::right( 123 ) ) );
	}

	/**
	 * @test
	 */
	public function test_isLeft() {
		$this->assertFalse( Fns::isLeft( 123 ) );
		$this->assertTrue( Fns::isLeft( Either::left( 123 ) ) );
	}

	/**
	 * @test
	 */
	public function test_isJust() {
		$this->assertFalse( Fns::isJust( 123 ) );
		$this->assertTrue( Fns::isJust( Maybe::fromNullable( 123 ) ) );
	}

	/**
	 * @test
	 */
	public function test_isNothing() {
		$this->assertFalse( Fns::isNothing( null ) );
		$this->assertTrue( Fns::isNothing( Maybe::fromNullable( null ) ) );
	}

	/**
	 * @test
	 */
	public function test_tap() {
		$value = 'some value';

		$mock = $this->getMockBuilder( '\stdClass' )->setMethods( [ 'doSth' ] )->getMock();
		$mock->expects( $this->once() )->method( 'doSth' )->with( $value );

		$this->assertEquals( $value, Fns::tap( [ $mock, 'doSth' ], $value ) );
	}

	/**
	 * @test
	 */
	public function test_for_each() {
		$values = [ 'a', 'b', 'c' ];

		$mock = $this->getMockBuilder( '\stdClass' )->setMethods( [ 'doSth' ] )->getMock();
		$mock->expects( $this->at( 0 ) )->method( 'doSth' )->with( 'a' );
		$mock->expects( $this->at( 1 ) )->method( 'doSth' )->with( 'b' );
		$mock->expects( $this->at( 2 ) )->method( 'doSth' )->with( 'c' );

		$this->assertEquals( $values, Fns::each( [ $mock, 'doSth' ], $values ) );
	}

	/**
	 * @test
	 */
	public function it_can_map() {
		$double = function ( $x ) { return 2 * $x; };

		$this->assertEquals( [ 2, 4, 6, 8 ], Fns::map( $double, [ 1, 2, 3, 4 ] ) );
	}

	/**
	 * @test
	 */
	public function it_can_map_with_key() {
		$this->assertEquals( [ 'a' => 'ba', 'c' => 'dc' ], Fns::map( Str::concat(), [ 'a' => 'b', 'c' => 'd' ] ) );
	}

	/**
	 * @test
	 */
	public function safe_returns_maybe() {
		$value = 123;
		$array = [ $value ];

		$safeNth = Fns::safe( Lst::nth() );

		$this->assertInstanceOf( Just::class, $safeNth( 0, $array ) );
		$this->assertEquals( $value, $safeNth( 0, $array )->getOrElse( 'failed' ) );

		$this->assertInstanceOf( Nothing::class, $safeNth( 1, $array ) );
		$this->assertEquals( 'failed', $safeNth( 1, $array )->getOrElse( 'failed' ) );
	}

	/**
	 * @test
	 */
	public function it_makes() {

		FunctionMocker::replace( '\WPML\Container\make', function ( $class, $args = [] ) {
			return (object) [ 'name' => $class, 'args' => $args ];
		} );

		$class = 'SomeClass';

		$instance = Fns::make( $class );

		$this->assertEquals( $class, $instance->name );
		$this->assertEquals( [], $instance->args );
	}

	/**
	 * @test
	 */
	public function it_makes_with_args() {

		FunctionMocker::replace( '\WPML\Container\make', function ( $class, $args = [] ) {
			return (object) [ 'name' => $class, 'args' => $args ];
		} );

		$class = 'SomeClass';
		$arg1  = 'arg1';
		$arg2  = 'arg2';
		$arg3  = 'arg3';

		$assertOk = function ( $instance ) use ( $class, $arg1, $arg2, $arg3 ) {
			$this->assertEquals( $class, $instance->name );
			$this->assertEquals( [ $arg1, $arg2, $arg3 ], $instance->args );
		};

		$W = [ Wrapper::class, 'of' ];
		$assertOk( Fns::makeN( 3, $class, $arg1, $arg2, $arg3 ) );
		$assertOk( $W( Fns::makeN( 3, $class ) )->ap( $arg1 )->ap( $arg2 )->ap( $arg3 )->get() );
		$assertOk( $W( Fns::makeN( 3, $class, $arg1, Fns::__, $arg3 ) )->ap( $arg2 )->get() );
	}

	/**
	 * @test
	 */
	public function test_unary() {
		$fn = function ( ...$args ) {
			return array_sum( $args );
		};

		$this->assertEquals( 6, $fn( 1, 2, 3 ) );

		$fn = Fns::unary( $fn );

		$this->assertEquals( 1, $fn( 1, 2, 3 ) );
	}

	/**
	 * @test
	 */
	public function it_memorizes() {
		$count     = 0;
		$factorial = Fns::memorizeWith( Fns::identity(), function ( $n ) use ( &$count ) {
			$count ++;

			return Math::product( Lst::range( 1, $n ) );
		} );

		$this->assertEquals( 120, $factorial( 5 ) );
		$this->assertEquals( 120, $factorial( 5 ) );
		$this->assertEquals( 120, $factorial( 5 ) );

		$this->assertEquals( 1, $count );
	}

	/**
	 * @test
	 */
	public function it_memorizes_args() {
		$count     = 0;
		$factorial = Fns::memorize( function ( $n ) use ( &$count ) {
			$count ++;

			return Math::product( Lst::range( 1, $n ) );
		} );

		$this->assertEquals( 120, $factorial( 5 ) );
		$this->assertEquals( 120, $factorial( 5 ) );
		$this->assertEquals( 120, $factorial( 5 ) );

		$this->assertEquals( 1, $count );

		$count     = 0;
		$add = Fns::memorize( function ( $a, $b ) use ( &$count ) {
			$count ++;

			return $a + $b;
		} );

		$this->assertEquals( 3, $add( 1, 2 ) );
		$this->assertEquals( 3, $add( 1, 2 ) );
		$this->assertEquals( 3, $add( 1, 2 ) );

		$this->assertEquals( 1, $count );

		$this->assertEquals( 4, $add( 2, 2 ) );
		$this->assertEquals( 4, $add( 2, 2 ) );

		$this->assertEquals( 2, $count );

		$this->assertEquals( 3, $add( 2, 1 ) );
		$this->assertEquals( 3, $add( 2, 1 ) );

		$this->assertEquals( 3, $count );

		$count     = 0;
		$addWithObjectArgs = Fns::memorize( function ( $a, $b ) use ( &$count ) {
			$count ++;

			return $a->value + $b->value;
		} );

		$this->assertEquals( 3, $addWithObjectArgs( (object)[ 'value' => 1 ], (object)[ 'value' => 2 ] ) );
		$this->assertEquals( 3, $addWithObjectArgs( (object)[ 'value' => 1 ], (object)[ 'value' => 2 ] ) );
		$this->assertEquals( 3, $addWithObjectArgs( (object)[ 'value' => 1 ], (object)[ 'value' => 2 ] ) );

		$this->assertEquals( 1, $count );

		$count     = 0;
		$addWithArrayArgs = Fns::memorize( function ( $a, $b ) use ( &$count ) {
			$count ++;

			return $a['value'] + $b['value'];
		} );

		$this->assertEquals( 3, $addWithArrayArgs( [ 'value' => 1 ], [ 'value' => 2 ] ) );
		$this->assertEquals( 3, $addWithArrayArgs( [ 'value' => 1 ], [ 'value' => 2 ] ) );
		$this->assertEquals( 3, $addWithArrayArgs( [ 'value' => 1 ], [ 'value' => 2 ] ) );

		$this->assertEquals( 1, $count );

	}

	/**
	 * @test
	 */
	public function it_memorizes_when_function_has_null_return_value() {
		$count = 0;

		$doSomething = Fns::memorize( function( $args ) use ( &$count ) {
			$count++;
		} );

		$doSomething( 'foo' );
		$this->assertSame( 1, $count );
		$doSomething( 'foo' );
		$this->assertSame( 1, $count );

		$doSomething( 'bar' );
		$this->assertSame( 2, $count );
		$doSomething( 'bar' );
		$this->assertSame( 2, $count );
	}

	/**
	 * @test
	 */
	public function test_once() {
		$addOneOnce = Fns::once( function ( $x ) { return $x + 1; } );

		$this->assertEquals( 11, $addOneOnce( 10 ) );
		$this->assertEquals( 11, $addOneOnce( $addOneOnce( 50 ) ) );

		$nullOnTen = Fns::once( function ( $x ) { return $x === 10 ? null : $x + 1; } );
		$this->assertNull( $nullOnTen( 10 ) );
		$this->assertNull( $nullOnTen( 1 ) );
	}

	/**
	 * @test
	 */
	public function test_withoutRecursion() {
		$addOne = Fns::withoutRecursion(
			function ( $x ) { return "Recursion halted with {$x}"; },
			function ( $x ) use ( &$addOne ) { return $addOne( $x + 1 ); }
		);

		$this->assertEquals( "Recursion halted with 11", $addOne( 10 ) );
		$this->assertEquals( "Recursion halted with 13", $addOne( 12 ) );
	}

	/**
	 * @test
	 */
	public function test_liftA2() {

		$add = curryN( 2, function ( $x, $y ) { return $x + $y; } );

		$this->assertEquals( Just::of( 10 ), Fns::liftA2( $add, Just::of( 7 ), Just::of( 3 ) ) );
		$this->assertEquals( Maybe::nothing(), Fns::liftA2( $add, Just::of( 7 ), Maybe::nothing() ) );
		$this->assertEquals( Maybe::nothing(), Fns::liftA2( $add, Maybe::nothing(), Just::of( 3 ) ) );

		$this->assertEquals( Right::of( 10 ), Fns::liftA2( $add, Right::of( 7 ), Right::of( 3 ) ) );
		$this->assertEquals( Either::left( 'error' ), Fns::liftA2( $add, Right::of( 7 ), Either::left( 'error' ) ) );
		$this->assertEquals( Either::left( 'error' ), Fns::liftA2( $add, Either::left( 'error' ), Right::of( 3 ) ) );

		$liftedAdd = Fns::liftA2( $add );
		$this->assertEquals( Just::of( 10 ), $liftedAdd( Just::of( 7 ), Just::of( 3 ) ) );

	}

	/**
	 * @test
	 */
	public function test_liftA3() {

		$add = curryN( 3, function ( $x, $y, $z ) { return $x + $y + $z; } );

		$this->assertEquals( Just::of( 10 ), Fns::liftA3( $add, Just::of( 7 ), Just::of( 1 ), Just::of( 2 ) ) );
		$this->assertEquals( Maybe::nothing(), Fns::liftA3( $add, Just::of( 7 ), Just::of( 1 ), Maybe::nothing() ) );

		$this->assertEquals( Right::of( 10 ), Fns::liftA3( $add, Right::of( 7 ), Right::of( 1 ), Right::of( 2 ) ) );
		$this->assertEquals( Either::left( 'error' ), Fns::liftA3( $add, Right::of( 7 ), Right::of( 1 ), Either::left( 'error' ) ) );

		$liftedAdd = Fns::liftA3( $add );
		$this->assertEquals( Just::of( 10 ), $liftedAdd( Just::of( 7 ), Just::of( 1 ), Just::of( 2 ) ) );

	}

	/**
	 * @test
	 */
	public function test_liftN() {

		$add = function ( $x, $y, $z ) { return $x + $y + $z; }; // NOTE: no need to explicitly curry

		$this->assertEquals( Just::of( 10 ), Fns::liftN( 3, $add, Just::of( 7 ), Just::of( 1 ), Just::of( 2 ) ) );
		$this->assertEquals( Maybe::nothing(), Fns::liftN( 3, $add, Just::of( 7 ), Just::of( 1 ), Maybe::nothing() ) );

		$this->assertEquals( Right::of( 10 ), Fns::liftN( 3, $add, Right::of( 7 ), Right::of( 1 ), Right::of( 2 ) ) );
		$this->assertEquals( Either::left( 'error' ), Fns::liftN( 3, $add, Right::of( 7 ), Right::of( 1 ), Either::left( 'error' ) ) );

		$liftedAdd = Fns::liftN( 3, $add );
		$this->assertEquals( Just::of( 10 ), $liftedAdd( Just::of( 7 ), Just::of( 1 ), Just::of( 2 ) ) );

		$liftedAddAndApplied2 = Fns::liftN( 3, $add, Just::of( 7 ), Just::of( 1 ) );
		$this->assertEquals( Just::of( 10 ), $liftedAddAndApplied2( Just::of( 2 ) ) );

	}

	public function test_withNamedLock() {
		$lockName = 'my-lock';

		$addOne = Fns::withNamedLock(
			$lockName,
			Fns::identity(),
			function ( $x ) use ( &$addOne ) { return $addOne( $x + 1 ); }
		);

		$this->assertEquals( 13, $addOne( 12 ), 'Should not recurse' );

		$addTwo = Fns::withNamedLock(
			$lockName,
			Fns::identity(),
			function ( $x ) use ( $addOne ) {
				$fn = pipe( $addOne, $addOne );

				return $fn( $x );
			}
		);

		$this->assertEquals( 10, $addTwo( 10 ), 'Should return 10 because $addOne is locked by same name as $addTwo' );
	}

	/**
	 * @test
	 */
	public function test_until() {
		$fns = [
			Fns::always( null ),
			Fns::always( null ),
			Fns::always( 123 ),
			Fns::always( 456 ),
			Fns::always( 890 ),
		];

		$untilNotNull = Fns::until( Logic::isNotNull(), $fns );
		$this->assertSame( 123, $untilNotNull(1) );

		$untilGreaterThan300 = Fns::until( Relation::gt( Fns::__, 300 ), $fns );
		$this->assertSame( 456, $untilGreaterThan300( 1 ) );

		$add = curryN( 2, function ( $a, $b ) {
			return $a + $b;
		} );

		$fns = [
			$add(1),
			$add(5),
			$add(10),
			$add(23),
		];

		$untilGreaterThan18 = Fns::until( Relation::gt( Fns::__, 18 ), $fns );
		$this->assertSame( 20, $untilGreaterThan18( 10 ) );
	}

	/**
	 * @test
	 */
	public function it_transforms_maybe_to_either() {
		$value = 10;
		$or = 20;

		$this->assertEquals(
			Either::of( $value ),
			Fns::maybeToEither( $or, Maybe::of( $value ) )
		);

		$this->assertEquals(
			Either::left( $or ),
			Fns::maybeToEither( $or, Maybe::nothing() )
		);
	}

}

class constructNTestClass {

	public function __construct( $data1, $data2 ) {
		$this->data1 = $data1;
		$this->data2 = $data2;
	}

	public function get() {
		return [ $this->data1, $this->data2 ];
	}
}
