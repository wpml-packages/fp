<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class JustTest extends TestCase {

	/**
	 * @test
	 */
	public function it_maps() {
		$value = 'something';

		$just = new Just( $value );
		$this->assertEquals( strtoupper( $value ), $just->map( 'strtoupper' )->get() );
	}

	/**
	 * @test
	 */
	public function it_returns_value_for_getOrElse() {
		$value = 'something';

		$just = new Just( $value );
		$this->assertEquals( $value, $just->getOrElse( 'else' ) );
	}

	/**
	 * @test
	 */
	public function it_returns_value_on_filter_true() {
		$value = 10;
		$just  = new Just( $value );

		$filtered = $just->filter( function ( $x ) {
			return $x > 5;
		} );

		$this->assertEquals( $value, $filtered->get() );
		$this->assertTrue( $filtered->isJust() );
	}

	/**
	 * @test
	 */
	public function it_returns_nothing_on_filter_false() {
		$value    = 1;
		$just     = new Just( $value );
		$filtered = $just->filter( function ( $x ) {
			return $x > 5;
		} );

		$this->assertTrue( $filtered->isNothing() );
	}

	/**
	 * @test
	 */
	public function it_returns_unwrapped_value_on_chain() {
		$value  = 10;
		$just   = new Just( $value );
		$result = $just->chain( function ( $x ) {
			return $x * 2;
		} );

		$this->assertEquals( $value * 2, $result );
	}

	/**
	 * @test
	 */
	public function it_uses_identity_for_default_filter() {

		wpml_collect( [ 10, 'string', [ 'array' => 'data' ] ] )->each(
			function ( $value ) {
				$just     = new Just( $value );
				$filtered = $just->filter();
				$this->assertEquals( $value, $filtered->get() );
			}
		);

		wpml_collect( [ 0, '', [], null ] )->each(
			function ( $value ) {
				$just     = new Just( $value );
				$filtered = $just->filter();
				$this->assertTrue( $filtered->isNothing() );
			}
		);

	}

	/**
	 * @test
	 */
	public function it_has_applicative() {
		$string = 'test';

		$just = Just::of( 'strtoupper' );
		$this->assertEquals( strtoupper( $string ), $just->ap( Just::of( $string ) )->get() );
	}

}
