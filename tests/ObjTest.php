<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;
use WPML\Collect\Support\Collection;

class ObjTest extends TestCase {

	/**
	 * @test
	 */
	public function it_gets_prop() {
		$value      = 'value';
		$key        = 'key';
		$array      = [ $key => $value ];
		$collection = wpml_collect( $array );
		$obj        = (object) $array;

		$prop = Obj::prop();
		$this->assertEquals( $value, $prop( $key, $array ) );
		$this->assertEquals( $value, $prop( $key, $collection ) );
		$this->assertEquals( $value, $prop( $key, $obj ) );

		$this->assertNull( $prop( 'wrong-key', $array ) );
		$this->assertNull( $prop( 'wrong-key', $collection ) );
		$this->assertNull( $prop( 'wrong-key', $obj ) );

		$propKey = Obj::prop( $key );
		$this->assertEquals( $value, $propKey( $array ) );
		$this->assertEquals( $value, $propKey( $collection ) );
		$this->assertEquals( $value, $propKey( $obj ) );

		$propWrongKey = Obj::prop( 'wrong-key' );
		$this->assertNull( $propWrongKey( $array ) );
		$this->assertNull( $propWrongKey( $collection ) );
		$this->assertNull( $propWrongKey( $obj ) );

		$this->assertEquals( $value, Obj::prop( $key, $array ) );
		$this->assertEquals( $value, Obj::prop( $key, $collection ) );
		$this->assertEquals( $value, Obj::prop( $key, $obj ) );

		$this->assertNull( Obj::prop( 'wrong-key', $array ) );
		$this->assertNull( Obj::prop( 'wrong-key', $collection ) );
		$this->assertNull( Obj::prop( 'wrong-key', $obj ) );
	}

	/**
	 * @test
	 */
	public function it_gets_prop_from_object() {
		$object  = new TestObject();
		$default = 'default';

		$this->assertSame( 'property_value', Obj::propOr( $default, 'property', $object ), 'property defined in class' );
		$this->assertNull( Obj::propOr( $default, 'property2', $object ), 'property exists but it is not initialized' );
		$this->assertSame( $default, Obj::propOr( $default, 'property3', $object ), 'property doesn\'t exist' );
		$this->assertSame( 'value1', Obj::propOr( $default, 'test1', $object ), 'property defined in __get' );
		$this->assertSame( 'value2', Obj::propOr( $default, 'test2', $object ), 'property defined in __get' );
		$this->assertSame( 5, Obj::propOr( $default, 'test3', $object ), 'property defined in __get' );
		$this->assertNull( Obj::propOr( $default, 'test4', $object ), 'property value is set to null' );
		$this->assertEquals( $default, Obj::propOr( $default, 'test5', $object ), 'property doesn\'t exist' );

		$numericProp            = 123;
		$objWithNumericProperty = (object) [ $numericProp => 'some value' ];
		$this->assertEquals( 'some value', Obj::propOr( null, $numericProp, $objWithNumericProperty ) );
	}

	/**
	 * @test
	 */
	public function it_gets_props() {
		$this->assertEquals( [ 1, 2 ], Obj::props( [ 'x', 'y' ], [ 'x' => 1, 'y' => 2 ] ) );
		$this->assertEquals( [ null, 1, 2 ], Obj::props( [ 'c', 'a', 'b' ], [ 'b' => 2, 'a' => 1 ] ) );
	}

	/**
	 * @test
	 */
	public function it_gets_prop_by_path() {
		$expected = 678;
		$item     = Collection::make( [
			'a' => 123,
			'b' => [
				'b_1' => (object) [
					'b_1_1' => 145,
					'b_1_2' => $expected,
				],
				'b_2' => 789,
			],
			'c' => [
				'c_1' => 111,
			],
		] );

		$this->assertEquals( $expected, Obj::path( [ 'b', 'b_1', 'b_1_2' ], $item ) );
		$this->assertEquals( null, Obj::path( [ 'b', 'b_1', 'xxxx' ], $item ) );
	}

	/**
	 * @test
	 * @see https://onthegosystems.myjetbrains.com/youtrack/issue/wpmlpb-216#focus=Comments-102-491203.0-0
	 */
	public function it_gets_by_invalid_path_and_throws_an_error() {
		$this->expectException( \InvalidArgumentException::class );

		$item = [
			'a' => 123,
			'b' => [
				'b_1' => 'string', // it should be `array|object|null`
			],
		];

		Obj::path( [ 'b', 'b_1', 'b_1_1' ], $item );
	}

	/**
	 * @test
	 */
	public function it_gets_prop_by_pathOr() {
		$expected = 678;
		$item     = Collection::make( [
			'a' => 123,
			'b' => [
				'b_1' => (object) [
					'b_1_1' => 145,
					'b_1_2' => $expected,
				],
				'b_2' => 789,
			],
			'c' => [
				'c_1' => 111,
				'c_2' => null,
			],
			'd' => '',
		] );

		$this->assertEquals( $expected, Obj::pathOr( 'Not found', [ 'b', 'b_1', 'b_1_2' ], $item ) );

		// test with undefined index
		$this->assertEquals( 'Not found', Obj::pathOr( 'Not found', [ 'b', 'b_1', 'xxxx' ], $item ) );

		// test with first path returning a non object or array
		$this->assertEquals( 'Not found', Obj::pathOr( 'Not found', [ 'd', 'd_1' ], $item ) );
	}

	/**
	 * @test
	 */
	public function it_sets_prop_by_path() {
		$expected = 1024;
		$item     = Collection::make( [
			'a' => 123,
			'b' => [
				'b_1' => (object) [
					'b_1_1' => 145,
					'b_1_2' => 678,
				],
				'b_2' => 789,
			],
			'c' => [
				'c_1' => 111,
			],
		] );

		$newItem = Obj::assocPath( [ 'b', 'b_1', 'b_1_2' ], $expected, $item );
		$this->assertEquals( $expected, $newItem->get( 'b' )['b_1']->b_1_2 );
	}

	/**
	 * @test
	 */
	public function it_throws_an_exception_on_wrong_prop_item() {
		$this->expectException( \InvalidArgumentException::class );
		Obj::prop( 'anything', 1 );
	}

	/**
	 * @test
	 */
	public function it_overrides_item_in_collection() {
		$collection = \wpml_collect( [
			'key1' => 'val1',
			'key2' => 123,
			'key3' => 'val3',
		] );

		$expectedCollection = \wpml_collect( [
			'key1' => 'val1',
			'key2' => 567,
			'key3' => 'val3',
		] );

		$this->assertEquals( $expectedCollection, Obj::assoc( 'key2', 567, $collection ) );

		// the original collection has not been changed
		$this->assertEquals( 123, $collection->get( 'key2' ) );
	}

	/**
	 * @test
	 */
	public function it_overrides_item_in_array() {
		$array = [
			'key1' => 'val1',
			'key2' => 123,
			'key3' => 'val3',
		];

		$expectedArray = [
			'key1' => 'val1',
			'key2' => 567,
			'key3' => 'val3',
		];

		$this->assertEquals( $expectedArray, Obj::assoc( 'key2', 567, $array ) );

		// the original array has not been changed
		$this->assertEquals( 123, $array['key2'] );
	}

	/**
	 * @test
	 */
	public function it_overrides_object_property() {
		$obj = (object) [
			'key1' => 'val1',
			'key2' => 123,
			'key3' => 'val3',
		];

		$expectedObj = (object) [
			'key1' => 'val1',
			'key2' => 567,
			'key3' => 'val3',
		];

		$this->assertEquals( $expectedObj, Obj::assoc( 'key2', 567, $obj ) );

		// the original object has not been changed
		$this->assertEquals( 123, $obj->key2 );
	}

	/**
	 * @test
	 */
	public function it_gets_property_via_lens() {
		$obj = (object) [
			'key1' => 'val1',
			'key2' => 123,
			'key3' => 'val3',
		];

		$lens = Obj::lens( Obj::prop( 'key2' ), Obj::assoc( 'key2' ) );

		$this->assertEquals( 123, Obj::view( $lens, $obj ) );
	}

	/**
	 * @test
	 */
	public function it_sets_property_via_lens() {
		$obj = (object) [
			'key1' => 'val1',
			'key2' => 123,
			'key3' => 'val3',
		];

		$lens   = Obj::lens( Obj::prop( 'key2' ), Obj::assoc( 'key2' ) );
		$newObj = Obj::set( $lens, 567, $obj );

		$this->assertEquals( 567, $newObj->key2 );
		$this->assertEquals( 123, $obj->key2 );
	}

	/**
	 * @test
	 */
	public function it_gets_property_via_lensProp() {
		$obj = (object) [
			'key1' => 'val1',
			'key2' => 123,
			'key3' => 'val3',
		];

		$lens = Obj::lensProp( 'key2' );

		$this->assertEquals( 123, Obj::view( $lens, $obj ) );
	}

	/**
	 * @test
	 */
	public function it_gets_property_via_lensPath() {
		$item = Collection::make( [
			'a' => 123,
			'b' => [
				'b_1' => (object) [
					'b_1_1' => 145,
					'b_1_2' => 678,
				],
				'b_2' => 789,
			],
			'c' => [
				'c_1' => 111,
			],
		] );

		$lens = Obj::lensPath( [ 'b', 'b_1', 'b_1_2' ] );

		$this->assertEquals( 678, Obj::view( $lens, $item ) );
	}

	/**
	 * @test
	 */
	public function it_sets_property_via_lensProp() {
		$obj = (object) [
			'key1' => 'val1',
			'key2' => 123,
			'key3' => 'val3',
		];

		$lens   = Obj::lensProp( 'key2' );
		$newObj = Obj::set( $lens, 567, $obj );

		$this->assertEquals( 567, $newObj->key2 );
		$this->assertEquals( 123, $obj->key2 );
	}

	/**
	 * @test
	 */
	public function it_sets_property_via_lensPath() {
		$item = Collection::make( [
			'a' => 123,
			'b' => [
				'b_1' => (object) [
					'b_1_1' => 145,
					'b_1_2' => 678,
				],
				'b_2' => 789,
			],
			'c' => [
				'c_1' => 111,
			],
		] );

		$lens   = Obj::lensPath( [ 'b', 'b_1', 'b_1_2' ] );
		$newObj = Obj::set( $lens, 1024, $item );

		$this->assertEquals( 1024, $newObj->get( 'b' )['b_1']->b_1_2 );
	}

	/**
	 * @test
	 */
	public function it_applies_transformation() {
		$obj = (object) [
			'key1' => 'val1',
			'key2' => 123,
			'key3' => 'val3',
		];

		$lens   = Obj::lensProp( 'key1' );
		$newObj = Obj::over( $lens, 'strtoupper', $obj );

		$this->assertEquals( 'VAL1', $newObj->key1 );
		$this->assertEquals( 'val1', $obj->key1 );
	}

	/**
	 * This test shows the behaviour of `over` when
	 * the lense is pointing to non-existing data.
	 * The lens getter returns `null` and the transformation
	 * is applied on `null`.
	 *
	 * @test
	 */
	public function it_applies_transformation_over_a_non_existing_property() {
		$obj = (object) [
			'key1' => 'some value',
		];

		$lens   = Obj::lensProp( 'foo' );
		$newObj = Obj::over( $lens, 'strtoupper', $obj );

		$this->assertObjectNotHasAttribute( 'foo', $obj );
		$this->assertObjectHasAttribute( 'foo', $newObj );
		$this->assertEquals( '', $newObj->foo ); // The transformation is applied on `null`.
	}

	/**
	 * @test
	 */
	public function it_uses_composed_lenses_to_get_property() {
		$game = [
			'name'      => 'Keep Talking and Nobody Explodes',
			'genres'    => [ 'Puzzle', 'VR' ],
			'publisher' => [
				'name'     => 'Steel Crate Games',
				'location' => 'Ottawa',
			],
		];

		$response = [
			'statusCode' => 200,
			'body'       => $game,
		];

		$gameName     = Obj::lensProp( 'name' );
		$body         = Obj::lensProp( 'body' );
		$bodyGameName = compose( $body, $gameName );

		$this->assertEquals( 'Keep Talking and Nobody Explodes', Obj::view( $bodyGameName, $response ) );

		$newResponse = Obj::set( $bodyGameName, 'New name', $response );
		$this->assertEquals( 'New name', $newResponse['body']['name'] );
	}

	/**
	 * @test
	 */
	public function it_can_propOr() {
		$alice = [
			'name' => 'ALICE',
		];

		$defaultAge = 101;

		$this->assertEquals( null, Obj::prop( 'age', $alice ) );
		$this->assertEquals( $defaultAge, Obj::propOr( $defaultAge, 'age', $alice ) );
	}

	/**
	 * @test
	 * @dataProvider dp_falsy_values
	 */
	public function it_can_propOr_falsy_values( $value ) {
		$arrayData = [
			'field' => $value,
		];

		$default = 101;

		$this->assertEquals( $value, Obj::propOr( $default, 'field', $arrayData ) );

		$collection = \wpml_collect( $arrayData );
		$this->assertEquals( $value, Obj::propOr( $default, 'field', $collection ) );

		$obj = (object) $arrayData;
		$this->assertEquals( $value, Obj::propOr( $default, 'field', $obj ) );
	}

	/**
	 * @test
	 */
	public function it_can_pick() {
		$products = [
			[ 'name' => 'Jeans', 'price' => 80, 'category' => 'clothes' ],
			(object) [ 'name' => 'Hoodie', 'price' => 60, 'category' => 'clothes' ],
			wpml_collect( [ 'name' => 'Jacket', 'price' => 120, 'category' => 'clothes' ] ),
			[ 'name' => 'Cards', 'price' => 35, 'category' => 'games' ],
			[ 'name' => 'Hat', 'category' => 'clothes' ],
		];

		$expected = [
			[ 'name' => 'Jeans', 'price' => 80 ],
			(object) [ 'name' => 'Hoodie', 'price' => 60 ],
			wpml_collect( [ 'name' => 'Jacket', 'price' => 120 ] ),
			[ 'name' => 'Cards', 'price' => 35 ],
			[ 'name' => 'Hat' ],
		];
		$this->assertEquals( $expected, FP::map( Obj::pick( [ 'name', 'price' ] ), $products ) );

		$this->assertEquals( wpml_collect( $expected ), FP::map( Obj::pick( [
			'name',
			'price'
		] ), wpml_collect( $products ) ) );
	}

	/**
	 * @test
	 * @dataProvider dp_falsy_values
	 *
	 * @param $encodeUrl
	 */
	public function it_can_pick_fields_with_falsy_values( $encodeUrl ) {
		$language = [
			'code'           => 'en',
			'english_name'   => 'English',
			'native_name'    => 'English',
			'default_locale' => 'en_US',
			'encode_url'     => $encodeUrl,
			'tag'            => 'en'
		];

		$expected = [
			'default_locale' => 'en_US',
			'encode_url'     => $encodeUrl,
			'tag'            => 'en'
		];

		$fields = [ 'default_locale', 'encode_url', 'tag' ];

		$this->assertEquals( $expected, Obj::pick( $fields, $language ) );
	}

	public function dp_falsy_values() {
		return [
			[ '0' ],
			[ 0 ],
			[ null ],
			[ false ],
		];
	}

	/**
	 * @test
	 */
	public function it_can_pickAll() {
		$products = [
			[ 'name' => 'Jeans', 'price' => 80, 'category' => 'clothes' ],
			(object) [ 'name' => 'Hoodie', 'price' => 60, 'category' => 'clothes' ],
			wpml_collect( [ 'name' => 'Jacket', 'price' => 120, 'category' => 'clothes' ] ),
			[ 'name' => 'Cards', 'price' => 35, 'category' => 'games' ],
			[ 'name' => 'Hat', 'category' => 'clothes' ],
		];

		$expected = [
			[ 'name' => 'Jeans', 'price' => 80 ],
			(object) [ 'name' => 'Hoodie', 'price' => 60 ],
			wpml_collect( [ 'name' => 'Jacket', 'price' => 120 ] ),
			[ 'name' => 'Cards', 'price' => 35 ],
			[ 'name' => 'Hat', 'price' => null ],
		];
		$this->assertEquals( $expected, FP::map( Obj::pickAll( [ 'name', 'price' ] ), $products ) );

		$this->assertEquals( wpml_collect( $expected ), FP::map( Obj::pickAll( [
			'name',
			'price'
		] ), wpml_collect( $products ) ) );
	}

	/**
	 * @test
	 */
	public function it_can_project() {
		$products = [
			[ 'name' => 'Jeans', 'price' => 80, 'category' => 'clothes' ],
			(object) [ 'name' => 'Hoodie', 'price' => 60, 'category' => 'clothes' ],
			wpml_collect( [ 'name' => 'Jacket', 'price' => 120, 'category' => 'clothes' ] ),
			[ 'name' => 'Cards', 'price' => 35, 'category' => 'games' ],
		];

		$expected = [
			[ 'name' => 'Jeans', 'price' => 80 ],
			(object) [ 'name' => 'Hoodie', 'price' => 60 ],
			wpml_collect( [ 'name' => 'Jacket', 'price' => 120 ] ),
			[ 'name' => 'Cards', 'price' => 35 ],
		];
		$this->assertEquals( $expected, Obj::project( [ 'name', 'price' ], $products ) );

		$this->assertEquals( wpml_collect( $expected ), Obj::project( [
			'name',
			'price'
		], wpml_collect( $products ) ) );
	}

	/**
	 * @test
	 */
	public function test_where() {
		$products = [
			[ 'name' => 'Jeans', 'price' => 80, 'category' => 'clothes' ],
			(object) [ 'name' => 'Hoodie', 'price' => 60, 'category' => 'clothes' ],
			wpml_collect( [ 'name' => 'Jacket', 'price' => 120, 'category' => 'clothes' ] ),
			[ 'name' => 'Cards', 'price' => 35, 'category' => 'games' ],
			[ 'name' => 'Nothing', 'price' => 35 ],
		];

		$where     = Obj::where( [ 'category' => Relation::equals( 'clothes' ) ] );
		$whereName = pipe( $where, Lst::pluck( 'name' ) );
		$this->assertEquals(
			[ 'Jeans', 'Hoodie', 'Jacket' ],
			$whereName( $products )
		);

		$where     = Obj::where( [
			'category' => Relation::equals( 'clothes' ),
			'price'    => Relation::lt( Fns::__, 100 ),
		] );
		$whereName = pipe( $where, Lst::pluck( 'name' ) );
		$this->assertEquals(
			[ 'Jeans', 'Hoodie' ],
			$whereName( $products )
		);
	}

	/**
	 * @test
	 */
	public function test_pickBy() {
		$product = [
			'name'           => 'widget',
			'price'          => 10,
			'avgRating'      => 4.5,
			'shippingWeight' => '2 lbs',
			'shippingCost'   => 2,
			'shippingMethod' => 'UPS'
		];

		$expectedNumerics = [
			'price'        => 10,
			'avgRating'    => 4.5,
			'shippingCost' => 2
		];
		$pickByNumeric    = Obj::pickBy( function ( $val ) { return is_numeric( $val ); } );

		$this->assertEquals( $expectedNumerics, $pickByNumeric( $product ) );
		$this->assertEquals( (object) $expectedNumerics, $pickByNumeric( (object) $product ) );
		$this->assertEquals( wpml_collect( $expectedNumerics ), $pickByNumeric( wpml_collect( $product ) ) );

		$expectedShipping = [
			'shippingWeight' => '2 lbs',
			'shippingCost'   => 2,
			'shippingMethod' => 'UPS'
		];
		$pickByShipping   = Obj::pickBy( function ( $val, $key ) { return Str::includes( 'shipping', $key ); } );
		$this->assertEquals( $expectedShipping, $pickByShipping( $product ) );
	}

	/**
	 * @test
	 */
	public function test_pickByKey() {
		$product = [
			'name'           => 'widget',
			'price'          => 10,
			'avgRating'      => 4.5,
			'shippingWeight' => '2 lbs',
			'shippingCost'   => 2,
			'shippingMethod' => 'UPS'
		];
		$expectedShipping = [
			'shippingWeight' => '2 lbs',
			'shippingCost'   => 2,
			'shippingMethod' => 'UPS'
		];
		$pickByShipping   = Obj::pickByKey( Str::includes( 'shipping' ) );
		$this->assertEquals( $expectedShipping, $pickByShipping( $product ) );
	}

	/**
	 * @test
	 */
	public function test_has() {
		$hasName = Obj::has( 'name' );
		$this->assertTrue( $hasName( [ 'name' => 'alice' ] ) );
		$this->assertTrue( $hasName( (object) [ 'name' => 'alice' ] ) );
		$this->assertTrue( $hasName( wpml_collect( [ 'name' => 'alice' ] ) ) );
		$this->assertTrue( $hasName( [ 'name' => 'bob' ] ) );
		$this->assertFalse( $hasName( [] ) );

		$point    = [ 'x' => 0, 'y' => 0 ];
		$pointHas = Obj::has( Fns::__, $point );
		$this->assertTrue( $pointHas( 'x' ) );
		$this->assertTrue( $pointHas( 'y' ) );
		$this->assertFalse( $pointHas( 'z' ) );
	}

	public function dp_hasPath() {
		return [
			'Path exists' => [
				'path' => ['translator', 'email'],
				'data' => [ 'translator' => [ 'email' => 'some' ] ],
				'expectedResult' => true,
			],
			'First level exists, second not exists' => [
				'path' => ['translator', 'email'],
				'data' => [ 'translator' => [ 'name' => 'some' ] ],
				'expectedResult' => false,
			],
			'No level not exists' => [
				'path' => ['translator', 'email'],
				'data' => [ 'user' => [ 'face' => 'some' ] ],
				'expectedResult' => false,
			],
			'Matching an element with null value.' => [
				'path' => ['translator', 'email'],
				'data' => [ 'translator' => [ 'email' => null ] ],
				'expectedResult' => true,
			],
		];
	}

	/**
	 * @test
	 * @dataProvider dp_hasPath
	 */
	public function test_hasPath( $path, $data, $expectedResult ) {
		$hasTranslatorEmail = Obj::hasPath( [ 'translator', 'email'] );
		$this->assertEquals( $expectedResult, $hasTranslatorEmail( $data ) );
		$this->assertEquals( $expectedResult, $hasTranslatorEmail( json_decode(json_encode( $data ) ) ) );
		$this->assertEquals( $expectedResult, $hasTranslatorEmail( wpml_collect( $data ) ) );

		$dataHas = Obj::hasPath( Fns::__, $data );
		$this->assertEquals( $expectedResult, $dataHas( $path ) );

		$this->assertEquals( $expectedResult, Obj::hasPath( $path, $data ) );
	}

	/**
	 * @test
	 */
	public function test_evolve() {
		$tomato          = [
			'firstName' => '  Tomato ',
			'data'      => [ 'elapsed' => 100, 'remaining' => 1400 ],
			'id'        => 123
		];
		$transformations = [
			'firstName' => Str::trim( ' ' ),
			'lastName'  => Str::trim( ' ' ), // Will not get invoked.
			'data'      => [ 'elapsed' => Math::add( 1 ), 'remaining' => Math::add( - 1 ) ]
		];

		$this->assertEquals(
			[
				'firstName' => 'Tomato',
				'data'      => [ 'elapsed' => 101, 'remaining' => 1399 ],
				'id'        => 123
			],
			Obj::evolve( $transformations, $tomato )
		);
		$this->assertEquals(
			(object ) [
				'firstName' => 'Tomato',
				'data'      => [ 'elapsed' => 101, 'remaining' => 1399 ],
				'id'        => 123
			],
			Obj::evolve( $transformations, (object) $tomato )
		);
		$this->assertEquals(
			wpml_collect( [
				'firstName' => 'Tomato',
				'data'      => [ 'elapsed' => 101, 'remaining' => 1399 ],
				'id'        => 123
			] ),
			Obj::evolve( $transformations, wpml_collect( $tomato ) )
		);

	}

	/**
	 * @test
	 */
	public function test_keys() {
		$this->assertEquals( [ 0, 1, 2 ], Obj::keys( [ 'a', 'b', 'c' ] ) );
		$this->assertEquals( [ 'a', 'b', 'c' ], Obj::keys( [ 'a' => 1, 'b' => 2, 'c' => 3 ] ) );

		$this->assertEquals( [ 0, 1, 2 ], Obj::keys( \wpml_collect( [ 'a', 'b', 'c' ] ) ) );
		$this->assertEquals( [ 'a', 'b', 'c' ], Obj::keys( \wpml_collect( [ 'a' => 1, 'b' => 2, 'c' => 3 ] ) ) );

		$this->assertEquals( [ 'a', 'b', 'c' ], Obj::keys( (object) [ 'a' => 1, 'b' => 2, 'c' => 3 ] ) );
		$this->assertEquals( [ 'c', 'd' ], Obj::keys( new TestProperties() ) );
	}

	/**
	 * @test
	 */
	public function test_values() {
		$this->assertEquals( [ 'a', 'b', 'c' ], Obj::values( [ 'a', 'b', 'c' ] ) );
		$this->assertEquals( [ 1, 2, 3 ], Obj::values( [ 'a' => 1, 'b' => 2, 'c' => 3 ] ) );

		$this->assertEquals( [ 'a', 'b', 'c' ], Obj::values( \wpml_collect( [ 'a', 'b', 'c' ] ) ) );
		$this->assertEquals( [ 1, 2, 3 ], Obj::values( \wpml_collect( [ 'a' => 1, 'b' => 2, 'c' => 3 ] ) ) );

		$this->assertEquals( [ 1, 2, 3 ], Obj::values( (object) [ 'a' => 1, 'b' => 2, 'c' => 3 ] ) );
		$this->assertEquals( [ 12, 'Lorem ipsum' ], Obj::values( new TestProperties() ) );
	}

	/**
	 * @test
	 */
	public function test_objOf() {
		$must   = Obj::objOf( 'must' );
		$values = [ 'foo', 'bar', 'baz' ];

		$this->assertEquals( [ 'must' => 'foo' ], $must( $values[0] ) );

		$matchPhrases = pipe( Fns::map( Obj::objOf( 'match_phrase' ) ), $must );

		$expected = [
			'must' => [
				[ 'match_phrase' => 'foo' ],
				[ 'match_phrase' => 'bar' ],
				[ 'match_phrase' => 'baz' ],
			],
		];

		$this->assertEquals( $expected, $matchPhrases( $values ) );
	}

	/**
	 * @test
	 */
	public function test_replaceRecursive() {
		$originalObj = [
			'job_id' => 123,
			'fields' => [
				[
					'data'     => 'Data 1',
					'finished' => 1,
					'tid'      => 8,
				],
				[
					'data'     => 'Data 2',
					'finished' => 1,
					'tid'      => 9,
				],
				[
					'data'     => 'Data 3',
					'finished' => 0,
					'tid'      => 10,
				],
			],
		];

		$updatedData = [
			'fields' => [
				[
					'data' => 'Data 1 up'
				],
				[
					'data' => 'Data 2'
				],
				[
					'data' => 'Data 3 up'
				],
			]
		];

		$expected = [
			'job_id' => 123,
			'fields' => [
				[
					'data'     => 'Data 1 up',
					'finished' => 1,
					'tid'      => 8,
				],
				[
					'data'     => 'Data 2',
					'finished' => 1,
					'tid'      => 9,
				],
				[
					'data'     => 'Data 3 up',
					'finished' => 0,
					'tid'      => 10,
				],
			],
		];

		$this->assertEquals( $expected, Obj::replaceRecursive( $updatedData, $originalObj ) );
	}

	/**
	 * @test
	 */
	public function test_without() {
		$obj = [ 'abc' => 10, 'xyz' => 20, 1 => 'one' ];

		$this->assertEquals( [ 'abc' => 10, 1 => 'one' ], Obj::without( 'xyz', $obj ) );
		$this->assertEquals( [ 'xyz' => 20, 1 => 'one' ], Obj::without( 'abc', $obj ) );
		$this->assertEquals( [ 'abc' => 10,'xyz' => 20 ], Obj::without( 1, $obj ) );
		$this->assertEquals( [ 'abc' => 10,'xyz' => 20 ], Obj::without( '1', $obj ) );
	}

	/**
	* @test
	*/
	public function test_toArray() {
		$arr        = [ 1, 'some', 'value', 3.43 ];
		$collection = \wpml_collect( $arr );
		$this->assertEquals( $arr, Obj::toArray( $collection ) );

		$arr = [ 'prop1' => 1, 'prop2' => 'some value' ];
		$obj = (object) $arr;
		$this->assertEquals( $arr, Obj::toArray( $obj ) );
	}

	/**
	 * @test
	 */
	public function it_adds_prop() {
		$displayName = Fns::converge( Str::wrap(), [
			Obj::prop( 'firstName' ),
			Obj::prop( 'lastName' ),
			Fns::always( ' ' )
		] );

		$arr    = [ 'firstName' => 'Joe', 'lastName' => 'Doe' ];
		$newArr = Obj::addProp( 'displayName', $displayName, $arr );
		$this->assertEquals( 'Joe Doe', $newArr['displayName'] );

		$obj    = (object) $arr;
		$newObj = Obj::addProp( 'displayName', $displayName, $obj );
		$this->assertEquals( 'Joe Doe', $newObj->displayName );
	}

	/**
	* @test
	*/
	public function it_removes_prop() {
		$arr    = [ 'firstName' => 'Joe', 'lastName' => 'Doe', 'displayName' => 'Joe Doe' ];
		$newArr = Obj::removeProp( 'displayName', $arr );
		$this->assertEquals( [ 'firstName' => 'Joe', 'lastName' => 'Doe' ], $newArr );
		$this->assertEquals( 'Joe Doe', $arr['displayName'], 'The original is not changed' );

		// try to remove unexisting prop
		$newArr = Obj::removeProp( 'unexistingProp', $arr );
		$this->assertEquals( $arr, $newArr );

		$obj    = (object) $arr;
		$newObj = Obj::removeProp( 'displayName', $obj );
		$this->assertEquals( (object) [ 'firstName' => 'Joe', 'lastName' => 'Doe' ], $newObj );
		$this->assertEquals( 'Joe Doe', $obj->displayName, 'The original is not changed' );

		// try to remove unexisting prop
		$newObj = Obj::removeProp( 'unexistingProp', $obj );
		$this->assertEquals( $obj, $newObj );
	}

	/**
	* @test
	*/
	public function it_renames_prop() {
		$arr    = [ 'firstName' => 'Joe', 'lastName' => 'Doe' ];
		$newArr = Obj::renameProp( 'lastName', 'last_name', $arr );
		$this->assertEquals( [ 'firstName' => 'Joe', 'last_name' => 'Doe' ], $newArr );

		$obj    = (object) $arr;
		$newObj = Obj::renameProp( 'lastName', 'last_name', $obj );
		$this->assertEquals( (object) [ 'firstName' => 'Joe', 'last_name' => 'Doe' ], $newObj );
	}

	/**
	 * @test
	 */
	public function it_applies_mapped_lens_with_composition() {
		$data = [
			'foo' => [
				[
					'bar' => '3',
					'xyz' => '3',
					'ids' => [ '2', '4' ],
				],
				[
					'bar' => '5',
					'xyz' => '5',
					'ids' => [ '6', '8' ],
				],
				[
					'bar' => '8',
					'xyz' => '8',
					'ids' => '10', // Should not be altered because not an array.
				],
			],
			'ignored' => '123',
		];

		$expectedData = [
			'foo' => [
				[
					'bar' => 3,
					'xyz' => '3',
					'ids' => [ 2, 4 ],
				],
				[
					'bar' => 5,
					'xyz' => '5',
					'ids' => [ 6, 8 ],
				],
				[
					'bar' => 8,
					'xyz' => '8',
					'ids' => '10', // Should not be altered because not an array.
				],
			],
			'ignored' => '123',
		];

		$eachFoos   = Obj::lensMappedProp( 'foo' );
		$eachFooBar = compose( $eachFoos, Obj::lensProp( 'bar' ) );
		$eachFooIds = compose( $eachFoos, Obj::lensMappedProp( 'ids' ) );

		$process = pipe(
			Obj::over( $eachFooBar, Cast::toInt() ),
			Obj::over( $eachFooIds, Cast::toInt() )
		);

		$this->assertNotSame( $data, $process( $data ) );
		$this->assertSame( $expectedData, $process( $data ) );
	}

	/**
	 * @test
	 */
	public function it_applies_mapped_lens_on_mappable_object() {
		$data = (object) [
			'foo' => wpml_collect( [ '3', '5', '8' ] ),
			'bar' => (object) [ '1', '2', '3' ], // Should not be altered because not mappable.
		];

		$eachFoos = Obj::lensMappedProp( 'foo' );
		$eachBars = Obj::lensMappedProp( 'bar' );

		$process = pipe(
			Obj::over( $eachFoos, Cast::toInt() ),
			Obj::over( $eachBars, Cast::toInt() )
		);

		$newData = $process( $data );

		$this->assertSame( [ 3, 5, 8 ], $newData->foo->toArray() );
		$this->assertSame( [ '1', '2', '3' ], (array) $newData->bar );
	}

	/**
	 * @test
	 */
	public function it_applies_double_mapped_lens() {
		$data = [
			[ '1', '2', '3' ],
			[ '4', '5', '6' ],
		];

		$expectedData = [
			[ 1, 2, 3 ],
			[ 4, 5, 6 ],
		];

		$eachArrayOfArray = compose( Obj::lensMapped(), Obj::lensMapped() );

		$process = Obj::over( $eachArrayOfArray, Cast::toInt() );

		$this->assertNotSame( $expectedData, $data );
		$this->assertSame( $expectedData, $process( $data ) );
	}

	/**
	 * @test
	 * @dataProvider dpMerge
	 */
	public function it_merges_two_objects_or_arrays( $item, $newData, $expected ) {
		$actual = Obj::merge( $newData, $item );
		$this->assertEquals( $expected, $actual );
	}

	public function dpMerge() {
		return [
			'the simplest merge when item is object and new data is array' => [
				(object) [ 'field1' => 'value1', 'field2' => 'value2' ],
				[ 'field1' => 'value1_up' ],
				(object) [ 'field1' => 'value1_up', 'field2' => 'value2' ],
			],

			'the simplest merge when item is array and new data is array' => [
				[ 'field1' => 'value1', 'field2' => 'value2' ],
				[ 'field1' => 'value1_up' ],
				[ 'field1' => 'value1_up', 'field2' => 'value2' ],
			],

			'the simplest merge when item is object and new data is object' => [
				(object) [ 'field1' => 'value1', 'field2' => 'value2' ],
				(object) [ 'field1' => 'value1_up' ],
				(object) [ 'field1' => 'value1_up', 'field2' => 'value2' ],
			],

			'the simplest merge when item is array and new data is object' => [
				[ 'field1' => 'value1', 'field2' => 'value2' ],
				(object) [ 'field1' => 'value1_up' ],
				[ 'field1' => 'value1_up', 'field2' => 'value2' ],
			],

			'update one field and append a new one' => [
				(object) [ 'field1' => 'value1', 'field2' => 'value2' ],
				[ 'field1' => 'value1_up', 'field3' => 'value3' ],
				(object) [ 'field1' => 'value1_up', 'field2' => 'value2', 'field3' => 'value3' ],
			],

			'update nested fields' => [
				(object) [ 'field1' => 'value1', 'field2' => (object) [ 'field21' => 'value21', 'field22' => 'value22' ] ],
				[ 'field2' => [ 'field21' => 'value21_up', 'field23' => 'value23' ] ],
				(object) [ 'field1' => 'value1', 'field2' => (object) [ 'field21' => 'value21_up', 'field22' => 'value22', 'field23' => 'value23' ] ],
			],
		];
	}
}

class TestProperties {

	private   $a = 10;
	protected $b = 11;
	public    $c = 12;
	public    $d = 'Lorem ipsum';

}
