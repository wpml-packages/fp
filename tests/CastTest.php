<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class CastTest extends TestCase {

	/**
	 * @test
	 */
	public function it_casts_to_bool() {
		$this->assertSame( true, Cast::toBool( true ) );
		$this->assertSame( false, Cast::toBool( false ) );

		$this->assertSame( true, Cast::toBool( 1 ) );
		$this->assertSame( false, Cast::toBool( 0 ) );

		$this->assertSame( true, Cast::toBool( 'true' ) );
		$this->assertSame( true, Cast::toBool( 'false' ) );

		$this->assertSame( true, Cast::toBool( '1' ) );
		$this->assertSame( false, Cast::toBool( '0' ) );

		$this->assertSame( true, Cast::toBool( [ 'something' ] ) );
		$this->assertSame( false, Cast::toBool( [] ) );
	}

	/**
	 * @test
	 */
	public function it_casts_to_int() {
		// Same as https://www.php.net/manual/en/function.intval.php#refsect1-function.intval-examples

		$this->assertSame( 42, Cast::toInt( 42 ) );
		$this->assertSame( 42, Cast::toInt( '42' ) );
		$this->assertSame( 4, Cast::toInt( 4.2 ) );
		$this->assertSame( 42, Cast::toInt( '+42' ) );
		$this->assertSame( 34, Cast::toInt( 042 ) );
		$this->assertSame( 10000000000, Cast::toInt( 1e10 ) );
		$this->assertSame( 1, Cast::toInt( true ) );
		$this->assertSame( 0, Cast::toInt( false ) );
	}

	/**
	 * @test
	 */
	public function it_casts_to_str() {
		$stringify = Cast::toStr();

		$object = new class() {
			public function __toString() {
				return 'hello';
			}
		};

		$this->assertSame( '123', $stringify( 123 ) );
		$this->assertSame( '1.23', $stringify( 1.23 ) );
		$this->assertSame( 'hello', $stringify( $object ) );
	}

	/**
	 * @test
	 */
	public function it_casts_to_arr() {
		$arrayify = Cast::toArr();

		$object = new class() {
			public $a = 'foo';
			public $b = 'bar';
		};

		$this->assertEquals( [ 123 ], $arrayify( 123 ) );
		$this->assertEquals( [ 123 ], $arrayify( [ 123 ] ) );
		$this->assertEquals( [ 'hello' ], $arrayify( 'hello' ) );
		$this->assertEquals( [ 'a' => 'foo', 'b' => 'bar' ], $arrayify( $object ) );
	}
}
