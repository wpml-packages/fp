<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;
use Exception;

class LeftTest extends TestCase {

	/**
	 * @test
	 */
	public function map_is_a_noop() {
		$value = 'something';

		$left   = Either::left( $value );
		$result = $left->map( 'strtoupper' );
		$this->assertSame( $left, $result );
	}

	/**
	 * @test
	 */
	public function getOrElse_returns_other() {
		$left = Either::left( 'something' );
		$this->assertEquals( 'other', $left->getOrElse( 'other' ) );
	}

	/**
	 * @test
	 */
	public function orElse_applies_the_function() {
		$left = Either::left( 'something' );
		$this->assertEquals( Either::right( 'SOMETHING' ), $left->orElse( 'strtoupper' ) );
	}

	/**
	 * @test
	 */
	public function chain_is_a_noop() {
		$left = Either::left( 'something' );
		$this->assertSame( $left, $left->chain( 'strtoupper' ) );
	}

	/**
	 * @test
	 */
	public function getOrElseThrow_throws_an_expection() {
		$throwValue = 'error';

		$this->expectException( Exception::class );
		$this->expectExceptionMessage( $throwValue );

		$left = Either::left( 'something' );
		$left->getOrElseThrow( $throwValue );
	}

	/**
	 * @test
	 */
	public function filter_is_a_noop() {
		$left = Either::left( 2 );
		$this->assertSame( $left, $left->filter( function ( $x ) {
			return $x > 1;
		} ) );
	}

	/**
	 * @test
	 */
	public function applicative_is_noop() {
		$string = 'test';

		$left = Either::left( 'error' );
		$this->assertEquals( 'error', $left->ap( Either::of( $string ) )->orElse( FP::identity() )->get() );
	}

}
