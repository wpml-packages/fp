<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class LogicTest extends TestCase {

	/**
	 * @test
	 */
	public function test_not() {
		$this->assertTrue( Logic::not( false ) );
		$this->assertFalse( Logic::not( true ) );

		$this->assertTrue( Logic::not( function () {
			return false;
		} ) );
		$this->assertFalse( Logic::not( function () {
			return true;
		} ) );
	}

	/**
	 * @test
	 */
	public function test_isNotNull() {
		$this->assertTrue( Logic::isNotNull( 'something' ) );
		$this->assertFalse( Logic::isNotNull( null ) );
	}

	/**
	 * @test
	 */
	public function test_ifElse() {
		$greaterThan10 = function ( $data ) {
			return $data > 10;
		};
		$add10         = function ( $data ) {
			return $data + 10;
		};
		$add2          = function ( $data ) {
			return $data + 2;
		};

		$add10or2 = Logic::ifElse( $greaterThan10, $add10, $add2 );
		$this->assertEquals( 7, $add10or2( 5 ) );
		$this->assertEquals( 21, $add10or2( 11 ) );
	}

	/**
	 * @test
	 */
	public function test_when() {
		$greaterThan10 = function ( $data ) {
			return $data > 10;
		};
		$add10         = function ( $data ) {
			return $data + 10;
		};

		$maybeAdd10 = Logic::when( $greaterThan10, $add10 );
		$this->assertEquals( 5, $maybeAdd10( 5 ) );
		$this->assertEquals( 21, $maybeAdd10( 11 ) );
	}

	/**
	 * @test
	 */
	public function test_unless() {
		$greaterThan10 = function ( $data ) {
			return $data > 10;
		};
		$add10         = function ( $data ) {
			return $data + 10;
		};

		$maybeAdd10 = Logic::unless( $greaterThan10, $add10 );
		$this->assertEquals( 15, $maybeAdd10( 5 ) );
		$this->assertEquals( 11, $maybeAdd10( 11 ) );
	}

	/**
	 * @test
	 */
	public function test_cond() {
		$freeze = function ( $temp ) {
			return $temp === 0;
		};
		$boil   = function ( $temp ) {
			return $temp === 100;
		};
		$true   = Fns::always( true );

		$waterTemp = Logic::cond( [
			[ $freeze, Fns::always( 'water freezes at 0°C' ) ],
			[ $boil, Fns::always( 'water boils at 100°C' ) ],
			[
				$true,
				function ( $temp ) {
					return 'nothing special happens at ' . $temp . '°C';
				}
			]
		] );

		$this->assertEquals( 'water freezes at 0°C', $waterTemp( 0 ) );
		$this->assertEquals( 'nothing special happens at 50°C', $waterTemp( 50 ) );
		$this->assertEquals( 'water boils at 100°C', $waterTemp( 100 ) );
	}

	/**
	 * @test
	 */
	public function test_both() {
		$gt10 = function ( $v ) {
			return $v > 10;
		};
		$lt20 = function ( $v ) {
			return $v < 20;
		};
		$test = Logic::both( $gt10, $lt20 );

		$this->assertTrue( $test( 15 ) );
		$this->assertFalse( $test( 30 ) );
	}

	/**
	 * @test
	 */
	public function test_allPass() {
		$isQueen         = Relation::propEq( 'rank', 'Q' );
		$isSpade         = Relation::propEq( 'suit', '♠︎' );
		$isQueenOfSpades = Logic::allPass( [ $isQueen, $isSpade ] );

		$this->assertFalse( $isQueenOfSpades( [ 'rank' => 'Q', 'suit' => '♣︎' ] ) );
		$this->assertTrue( $isQueenOfSpades( [ 'rank' => 'Q', 'suit' => '♠︎' ] ) );
	}

	/**
	 * @test
	 */
	public function test_anyPass() {
		$isClub      = Relation::propEq( 'suit', '♣' );
		$isSpade     = Relation::propEq( 'suit', '♠' );
		$isBlackCard = Logic::anyPass( [ $isClub, $isSpade ] );

		$this->assertTrue( $isBlackCard( [ 'rank' => '10', 'suit' => '♣' ] ) );
		$this->assertTrue( $isBlackCard( [ 'rank' => 'Q', 'suit' => '♠' ] ) );
		$this->assertFalse( $isBlackCard( [ 'rank' => 'Q', 'suit' => '♦' ] ) );
	}

	/**
	 * @test
	 */
	public function test_complement() {
		$isNotNull = Logic::complement( 'is_null' );
		$this->assertTrue( is_null( null ) );
		$this->assertFalse( $isNotNull( null ) );
		$this->assertFalse( is_null( 7 ) );
		$this->assertTrue( $isNotNull( 7 ) );
	}

	/**
	 * @test
	 */
	public function test_defaultTo() {
		$defaultTo42 = Logic::defaultTo( 42 );

		$this->assertEquals( 42, $defaultTo42( null ) );
		$this->assertFalse( $defaultTo42( false ) );
		$this->assertEquals( 'WPML', $defaultTo42( 'WPML' ) );
	}

	/**
	 * @test
	 */
	public function test_either() {
		$gt10 = function ( $x ) {
			return $x > 10;
		};
		$even = function ( $x ) {
			return $x % 2 === 0;
		};
		$f    = Logic::either( $gt10, $even );

		$this->assertTrue( $f( 101 ) );
		$this->assertTrue( $f( 8 ) );
		$this->assertFalse( $f( 9 ) );
	}

	/**
	 * @test
	 */
	public function test_until() {
		$until = Logic::until( Relation::gt( Fns::__, 100 ), Math::multiply( 2 ) );
		$this->assertEquals( 128, $until( 1 ) );
	}

	/**
	 * @test
	 */
	public function test_propSatisfies() {
		$cars = [
			[
				'name'  => 'suv',
				'doors' => 4,
				'mpg'   => 19,
			],
			[
				'name'  => 'sedan',
				'doors' => 4,
				'mpg'   => 30,
			],
			[
				'name'  => 'hybrid',
				'doors' => 4,
				'mpg'   => 37,
			],
			[
				'name'  => 'compact',
				'doors' => 2,
				'mpg'   => 32,
			],
		];

		$goodMilage = Logic::propSatisfies( Relation::gte( Fns::__, 30 ), 'mpg' );
		$fourDoors  = Relation::propEq( 'doors', 4 );

		$perfectCars = Logic::allPass( [ $goodMilage, $fourDoors ] );

		$this->assertEquals(
			[
				[
					'name'  => 'sedan',
					'doors' => 4,
					'mpg'   => 30,
				],
				[
					'name'  => 'hybrid',
					'doors' => 4,
					'mpg'   => 37,
				],
			],
			Fns::filter( $perfectCars, $cars ) );

	}

	/**
	 * @test
	 */
	public function test_isArray() {

		$this->assertTrue( Logic::isArray( [] ) );
		$this->assertFalse( Logic::isArray( 10 ) );
		$this->assertFalse( Logic::isArray( 'string' ) );

		$isArray = Logic::isArray();

		$this->assertTrue( $isArray( [] ) );

	}

	/**
	 * @test
	 */
	public function test_isMappable() {
		$this->assertTrue( Logic::isMappable( [] ) );
		$this->assertTrue( Logic::isMappable( wpml_collect() ) );
		$this->assertFalse( Logic::isMappable( 'not mappable' ) );
		$this->assertFalse( Logic::isMappable( new \stdClass() ) );

		$isMappable = Logic::isMappable();

		$this->assertTrue( $isMappable( wpml_collect() ) );
		$this->assertFalse( $isMappable( new \stdClass() ) );
	}

	/**
	 * @test
	 */
	public function test_isEmpty() {
		$this->assertTrue( Logic::isEmpty( '' ) );
		$this->assertTrue( Logic::isEmpty( [] ) );
		$this->assertTrue( Logic::isEmpty( \wpml_collect( [] ) ) );
		$this->assertTrue( Logic::isEmpty( null ) );
		$this->assertTrue( Logic::isEmpty( false ) );
		$this->assertTrue( Logic::isEmpty( 0 ) );
		$this->assertTrue( Logic::isEmpty( 0.0 ) );
		$this->assertTrue( Logic::isEmpty( '0' ) );

		$this->assertFalse( Logic::isEmpty( 'a' ) );
		$this->assertFalse( Logic::isEmpty( [ 1, 2 ] ) );
		$this->assertFalse( Logic::isEmpty( \wpml_collect( [ 'a', 1 ] ) ) );
	}


	/**
	 * @test
	 */
	public function test_isNotEmpty() {
		$this->assertTrue( Logic::isNotEmpty( 'a' ) );
		$this->assertTrue( Logic::isNotEmpty( [ 1, 2 ] ) );
		$this->assertTrue( Logic::isNotEmpty( \wpml_collect( [ 'a', 1 ] ) ) );

		$this->assertFalse( Logic::isNotEmpty( '' ) );
		$this->assertFalse( Logic::isNotEmpty( [] ) );
		$this->assertFalse( Logic::isNotEmpty( \wpml_collect( [] ) ) );
		$this->assertFalse( Logic::isNotEmpty( null ) );
		$this->assertFalse( Logic::isNotEmpty( false ) );
		$this->assertFalse( Logic::isNotEmpty( 0 ) );
		$this->assertFalse( Logic::isNotEmpty( 0.0 ) );
		$this->assertFalse( Logic::isNotEmpty( '0' ) );
	}

	/**
	 * @test
	 */
	public function test_firstSatisfying() {
		$fn = Logic::firstSatisfying( Logic::isTruthy(), [
			Obj::prop( 'a' ),
			Obj::prop( 'b' ),
			Obj::prop( 'c' ),
			Obj::prop( 'd' ),
		] );

		$data = (object) [ 'c' => 12, 'd' => 15 ];
		$this->assertEquals( 12, $fn( $data ) );

		$data = (object) [ 'test' => 'some data' ];
		$this->assertNull( $fn( $data ) );

		$greaterThan10 = Relation::gt( Fns::__, 10 );
		$fn            = Logic::firstSatisfying( $greaterThan10, [
			Obj::prop( 'a' ),
			Obj::prop( 'b' ),
			Obj::prop( 'c' ),
			Obj::prop( 'd' ),
		] );

		$data = (object) [ 'c' => 9, 'd' => 15 ];
		$this->assertEquals( 15, $fn( $data ) );
	}

	/**
	 * @test
	 */
	public function test_truthy() {
		$this->assertTrue( Logic::isTruthy( true ) );
		$this->assertTrue( Logic::isTruthy( 1 ) );
		$this->assertTrue( Logic::isTruthy( 1.23 ) );
		$this->assertTrue( Logic::isTruthy( [ 1, 2, 3 ] ) );
		$this->assertTrue( Logic::isTruthy( \wpml_collect( [ 1, 2, 3 ] ) ) );
		$this->assertTrue( Logic::isTruthy( (object) [ 'a' => 1, 'b' => 2 ] ) );

		$this->assertFalse( Logic::isTruthy( false ) );
		$this->assertFalse( Logic::isTruthy( null ) );
		$this->assertFalse( Logic::isTruthy( 0 ) );
		$this->assertFalse( Logic::isTruthy( 0.0 ) );
		$this->assertFalse( Logic::isTruthy( [] ) );
		$this->assertFalse( Logic::isTruthy( \wpml_collect( [] ) ) );
	}
}
