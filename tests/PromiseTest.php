<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;
use function WPML\FP\Strings\explodeToCollection;

class PromiseTest extends TestCase {

	/**
	 * @test
	 */
	public function it_resolves() {

		$promise = new Promise();
		$promise->then( 'strtoupper' );

		$data = 'my string';
		$this->assertEquals( 'MY STRING', $promise->resolve( $data ) );
	}

	/**
	 * @test
	 */
	public function it_resolves_multiples() {

		$promise = new Promise();
		$promise->then( 'strtoupper' )->then( explodeToCollection( ' ' ) );

		$data = 'my string';
		$this->assertEquals( wpml_collect( [ 'MY', 'STRING' ] ), $promise->resolve( $data ) );
	}

	/**
	 * @test
	 */
	public function it_resolves_using_either() {

		$promise = new Promise();
		$promise->then( 'strtoupper' );

		$data = Either::of( 'my string' );
		$this->assertEquals( Either::of( 'MY STRING' ), $promise->resolve( $data ) );
	}

	/**
	 * @test
	 */
	public function it_resolves_using_either_left_or_right() {
		$strLongerThan10 = function ( $str ) { return strlen( $str ) > 11 ? Either::right( $str ) : Either::left( 'string not long enough' ); };

		$promise = new Promise();
		$promise->then( 'strtoupper' )->then( $strLongerThan10 );

		$data = Either::of( 'my string' );
		$this->assertEquals(
			Either::left( 'string not long enough' ),
			$promise->resolve( $data )
		);

		$data = Either::of( 'my string longer than 10' );
		$this->assertEquals(
			Either::of( 'MY STRING LONGER THAN 10' ),
			$promise->resolve( $data )
		);
	}

	/**
	 * @test
	 */
	public function it_does_not_catch_either_right() {
		$promise = new Promise();
		$promise->then( 'strtoupper' )->then( 'strrev' )->onError( 'strtolower' );

		$data = Either::right( 'My String' );
		$this->assertEquals( Either::right( 'GNIRTS YM' ), $promise->resolve( $data ) );
	}

	/**
	 * @test
	 */
	public function it_does_catch_instead_of_then_for_either_left() {
		$promise = new Promise();
		$promise->then( 'strrev' )->onError( 'strtolower' );

		$data = Either::left( 'My String' );
		$this->assertEquals( Either::right( 'my string' ), $promise->resolve( $data ) );
	}

}
