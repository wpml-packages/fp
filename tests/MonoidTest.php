<?php

namespace WPML\FP\Monoid;

use PHPUnit\Framework\TestCase;

class MonoidTest extends TestCase {

	/**
	 * @test
	 */
	public function it_reduces() {
		$this->assertFalse( Any::of( [ false, false ] ) );
		$this->assertTrue( Any::of( [ false, true ] ) );
		$this->assertFalse( Any::of( [] ) );

		$this->assertFalse( All::of( [ false, true ] ) );
		$this->assertTrue( All::of( [ true, true ] ) );

		$this->assertEquals( 10, Sum::of( [ 1, 2, 3, 4 ] ) );

		$this->assertEquals( 'onetwothree', Str::of( [ 'one', 'two', 'three' ] ) );
	}


}
