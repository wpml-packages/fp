<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class JsonTest extends TestCase {

	/**
	 * @test
	 */
	public function it_decodes_json_to_collection() {
		$data = ['some' => 'data' ];
		$this->assertEquals( wpml_collect( $data ), call_user_func( Json::toCollection(), json_encode( $data ) ) );
		$this->assertEquals( wpml_collect( $data ), Json::toCollection( json_encode( $data ) ) );
	}

	/**
	 * @test
	 */
	public function it_returns_null_if_json_is_not_value() {
		$data = 'invalid json';
		$this->assertEquals( null, Json::toCollection( $data ) );
	}

	/**
	 * @test
	 */
	public function it_returns_objects_as_arrays() {
		$data = ['some' => 'data' ];
		$this->assertEquals( $data, call_user_func( Json::toArray(), json_encode( (object)$data ) ) );
		$this->assertEquals( $data, Json::toArray( json_encode( (object)$data ) ) );
	}

}
