<?php

namespace WPML\FP\Strings;

use PHPUnit\Framework\TestCase;
use WPML\FP\Str;

class StringsTest extends TestCase {

	/**
	 * @test
	 */
	public function it_left_trims() {
		$str  = '[test';
		$trim = '[';

		$lTrimSq = ltrimWith( $trim );
		$this->assertEquals( ltrim( $str, $trim ), $lTrimSq( $str ) );
	}

	/**
	 * @test
	 */
	public function it_right_trims() {
		$str  = 'test]';
		$trim = ']';

		$rTrimSq = rtrimWith( $trim );
		$this->assertEquals( rtrim( $str, $trim ), $rTrimSq( $str ) );
	}

	/**
	 * @test
	 */
	public function it_explodes_to_collection() {
		$str = 'test,one,two,three';

		$explodeOnComma = explodeToCollection( ',' );
		$exploded            = $explodeOnComma( $str );
		$this->assertEquals( 4, $exploded->count() );
		$this->assertEquals(
			[ 'test', 'one', 'two', 'three' ],
			$exploded->toArray()
		);
	}

	/**
	 * @test
	 */
	public function it_gets_tail() {
		$str = '?testing';

		$this->assertEquals( 'testing', Str::tail( $str ) );
	}

	/**
	 * @test
	 */
	public function it_splits() {
		$str = 'usr/local/bin/node';

		$this->assertEquals( [ 'usr', 'local', 'bin', 'node' ], Str::split( '/', $str ) );
	}

	/**
	 * @test
	 */
	public function it_parses() {
		$str = 'foo=bar&success=1';

		$parsedString = [
			'foo'     => 'bar',
			'success' => '1',
		];

		$parse = Str::parse();

		$this->assertEquals( $parsedString, $parse( $str ) );
	}

	/**
	 * @test
	 */
	public function it_includes() {
		$str = 'my string';
		$this->assertTrue( Str::includes( 'my', $str ) );
		$this->assertTrue( Str::includes( 'str', $str ) );
		$this->assertFalse( Str::includes( 'My', $str ) );
	}

	/**
	 * @test
	 */
	public function includes_works_with_collections() {
		$itIncludesNeedle = Str::includes( 'needle' );
		$haystacks = [
			'This is a haystack with a needle',
			'This is a haystack with a ball',
			'needle, is something that starts with the haystack.',
			'Th',
		];

		$result = wpml_collect( $haystacks )
			->reject( $itIncludesNeedle )
			->toArray();

		$this->assertEquals( [
			1 => 'This is a haystack with a ball',
			3 => 'Th',
		], $result );
	}

	/**
	 * @test
	 */
	public function sub_works_with_collections() {
		$stringSub = Str::sub( 10 );

		$haystacks = [
			'This is a haystack with a needle',
			'This is a haystack with a ball',
		];

		$result = wpml_collect( $haystacks )
			->map( $stringSub )
			->toArray();

		$this->assertEquals( [
			0 => 'haystack with a needle',
			1 => 'haystack with a ball',
		], $result );
	}

	/**
	 * @test
	 */
	public function it_trims() {
		$str = ' tomato   ';
		$this->assertEquals( 'tomato', Str::trim( ' ', $str ) );
	}

	/**
	 * @test
	 */
	public function it_concats() {
		$this->assertEquals( 'ABCDEF', Str::concat( 'ABC', 'DEF' ) );
		$this->assertEquals( '', Str::concat( '', '' ) );
	}

	/**
	 * @test
	 */
	public function it_gets_sub_string() {
		$this->assertEquals( 'sting', Str::sub( 2, 'testing' ) );
		$this->assertEquals( 'ng', Str::sub( - 2, 'testing' ) );
	}

	/**
	 * @test
	 */
	public function it_starts_with() {
		$this->assertTrue( Str::startsWith( 'test', 'testing' ) );
		$this->assertFalse( Str::startsWith( 'best', 'testing' ) );
	}

	/**
	 * @test
	 */
	public function it_ends_with() {
		$this->assertTrue( Str::endsWith( 'ing', 'testing' ) );
		$this->assertFalse( Str::endsWith( 'tin', 'testing' ) );
	}

	/**
	 * @test
	 */
	public function test_str_pos() {
		$this->assertEquals( 0, Str::pos( 'test', 'testing' ) );
		$this->assertEquals( 4, Str::pos( 'ing', 'testing' ) );
		$this->assertFalse( Str::pos( 'best', 'testing' ) );
	}

	/**
	 * @test
	 */
	public function it_gets_length() {
		$this->assertEquals( 0, Str::len( '' ) );
		$this->assertEquals( 4, Str::len( 'test' ) );
	}

	/**
	 * @test
	 */
	public function it_replaces() {
		$this->assertEquals( 'Left wing', Str::replace( 'Right', 'Left', 'Right wing' ) );
		$this->assertEquals( 'Right wrong', Str::replace( 'in', 'ron', 'Right wing' ) );
	}

	/**
	 * @test
	 */
	public function test_pregReplace() {
		$pattern  = '/<span>(.*?)<\/span>/';
		$replace  = '<strong>${1}</strong>';
		$subject  = 'Lorem <span>ipsum</span>';
		$expected = 'Lorem <strong>ipsum</strong>';

		$this->assertEquals( $expected, Str::pregReplace( $pattern, $replace, $subject ) );
	}

	/**
	 * @test
	 */
	public function test_match() {
		$pattern  = '/<span>(.*?)<\/span>/';
		$subject  = 'Lorem <span>ipsum</span>';
		$expected = [ '<span>ipsum</span>', 'ipsum' ];

		$this->assertEquals( $expected, Str::match( $pattern, $subject ) );
	}

	/**
	 * @test
	 */
	public function test_matchALl() {
		$pattern  = '/<span>(.*?)<\/span>/';
		$subject  = 'Lorem <span>foo</span> and <span>bar</span> end';
		$expected = [
			[ '<span>foo</span>', 'foo' ],
			[ '<span>bar</span>', 'bar' ],
		];

		$this->assertEquals( $expected, Str::matchAll( $pattern, $subject ) );
	}

	/**
	 * @test
	 */
	public function test_wrap() {
		$wrapsInDiv = Str::wrap( '<div>', '</div>' );

		$this->assertEquals( "<div>To be wrapped</div>", $wrapsInDiv( 'To be wrapped' ) );
	}

	/**
	 * @test
	 */
	public function test_toUpper() {
		$this->assertEquals( 'TEST', Str::toUpper( 'test' ) );
	}

	/**
	 * @test
	 */
	public function test_toLower() {
		$this->assertEquals( 'test', Str::toLower( 'TesT' ) );
	}

	/**
	 * @test
	 */
	public function test_trimPrefix() {
		$this->assertEquals( 'test', Str::trimPrefix( 'prefix-', 'prefix-test' ) );
		$this->assertEquals( 'test', Str::trimPrefix( 'prefix-', 'test' ) );
	}

	public function dp_test_truncate_bytes() {
		return [
			'Test single byte string' => [
				'max_bytes' => 5,
				'max_characters' => null,
				'string' => 'hello world',
				'expectedString' => 'hello',
				'expectedBytes' => 5,
				'expectedChars' => 5
			],
			'Test single byte string, max chars' => [
				'max_bytes' => 5,
				'max_characters' => 3,
				'string' => 'hello world',
				'expectedString' => 'hel',
				'expectedBytes' => 3,
				'expectedChars' => 3
			],
			'Test multibyte string' => [
				'max_bytes' => 6,
				'max_characters' => null,
				'string' => 'サンリオキ',
				'expectedString' => 'サン',
				'expectedBytes' => 6,
				'expectedChars' => 2
			],
			'Test multibyte string, lower max chars' => [
				'max_bytes' => 6,
				'max_characters' => 1,
				'string' => 'サンリオキ',
				'expectedString' => 'サ',
				'expectedBytes' => 3,
				'expectedChars' => 1
			],
			'Test multibyte string, higher max chars' => [
				'max_bytes' => 6,
				'max_characters' => 5,
				'string' => 'サンリオキ',
				'expectedString' => 'サン',
				'expectedBytes' => 6,
				'expectedChars' => 2
			],
		];
	}

	/**
	 * @test
	 * @dataProvider dp_test_truncate_bytes
	 */
	public function test_truncate_bytes( $max_bytes, $max_characters, $string, $expectedString, $expectedBytes, $expectedChars ) {
		// Use the helper function to truncate the string
		$truncated_string = Str::truncate_bytes( $string, $max_bytes, $max_characters );

		$this->assertEquals( $expectedString, $truncated_string );
		$this->assertEquals( $expectedBytes, strlen( $truncated_string ) );
		$this->assertEquals( $expectedChars, mb_strlen( $truncated_string ) );
	}

}
