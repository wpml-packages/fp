<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

/**
 * @group lens
 */
class LensTest extends TestCase {

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIso() {
		$temperatureInFahrenheit = 70;

		$f2c = function( $t ) {
			return ( $t - 32 ) * 5 / 9;
		};

		$c2f = function( $t ) {
			return ( $t * 9 / 5 ) + 32;
		};

		$lensInCelsius = Lens::iso( $f2c, $c2f );

		// View
		$this->assertSame(
			$f2c( $temperatureInFahrenheit ),
			Obj::view( $lensInCelsius, $temperatureInFahrenheit )
		);

		// Set
		$tempInCelsius                = 25;
		$tempInFahrenheitFor25Celsius = $c2f( $tempInCelsius );

		$this->assertEquals(
			$tempInFahrenheitFor25Celsius,
			Obj::set( $lensInCelsius, $tempInCelsius, $temperatureInFahrenheit )
		);

		// Over
		$add10Celsius                       = Math::add( 10 );
		$tempInFahrenheitWithAdded10Celsius = $c2f(	$add10Celsius( $f2c( $temperatureInFahrenheit ) ) );

		$this->assertSame(
			$tempInFahrenheitWithAdded10Celsius,
			Obj::over( $lensInCelsius, $add10Celsius, $temperatureInFahrenheit )
		);
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsoIdentity() {
		$value     = 123;
		$increment = 1;

		$this->assertSame(
			$value + $increment,
			Obj::over( Lens::isoIdentity(), Math::add( $increment ), $value )
		);
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsoUnserialized() {
		$value     = 123;
		$increment = 1;

		$getData = function( $value ) {
			return (object) [
				'foo' => serialize( (object) [
					'bar' => $value,
				] ),
			];
		};

		$lensOnBar = compose( Obj::lensProp( 'foo' ), Lens::isoUnserialized(), Obj::lensProp( 'bar' ) );

		$this->assertEquals(
			$getData( $value + $increment ),
			Obj::over( $lensOnBar, Math::add( $increment ), $getData( $value ) )
		);
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsoJsonDecoded() {
		$value     = 123;
		$increment = 1;

		$getData = function( $value ) {
			return (object) [
				'foo' => json_encode( (object) [
					'bar' => $value,
				] ),
			];
		};

		$lensOnBar = compose( Obj::lensProp( 'foo' ), Lens::isoJsonDecoded(), Obj::lensProp( 'bar' ) );

		$this->assertEquals(
			$getData( $value + $increment ),
			Obj::over( $lensOnBar, Math::add( $increment ), $getData( $value ) )
		);
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsoUrlDecoded() {
		$original    = 'How are you?';
		$translation = 'Comment vas-tu ?';

		$getData = function( $value ) {
			return (object) [
				'foo' => urlencode( "Some content before - $value - and after" ),
			];
		};

		$lensOnDecodedString = compose( Obj::lensProp( 'foo' ), Lens::isoUrlDecoded() );

		$this->assertEquals(
			$getData( $translation ),
			Obj::over( $lensOnDecodedString, Str::replace( $original, $translation ), $getData( $original ) )
		);
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsoBase64Decoded() {
		$original    = 'How are you?';
		$translation = 'Comment vas-tu ?';

		$getData = function( $value ) {
			return (object) [
				'foo' => base64_encode( "Some content before - $value - and after" ),
			];
		};

		$lensOnDecodedString = compose( Obj::lensProp( 'foo' ), Lens::isoBase64Decoded() );

		$this->assertEquals(
			$getData( $translation ),
			Obj::over( $lensOnDecodedString, Str::replace( $original, $translation ), $getData( $original ) )
		);
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsoParsedUrl() {
		$unsafeUrl = 'http://example.com/path/?lang=fr&good=1';
		$safeUrl   = 'https://example.com/path/?lang=fr&good=1';

		$getData = function( $url ) {
			return (object) [
				'url' => $url,
			];
		};

		$lensOnUrlScheme = compose( Obj::lensProp( 'url' ), Lens::isoParsedUrl(), Obj::lensProp( 'scheme' ) );

		$this->assertEquals(
			$getData( $safeUrl ),
			Obj::over( $lensOnUrlScheme, Fns::always( 'https' ), $getData( $unsafeUrl ) )
		);
	}

	/**
	 * @test
	 *
	 * @return void
	 */
	public function testIsoParsedQuery() {
		$langDefault   = 'en';
		$langSecondary = 'fr';

		$getData = function( $lang ) {
			return (object) [
				'query' => "foo=bar&lang=$lang&good=1",
			];
		};

		$lensOnQueryLang = compose( Obj::lensProp( 'query' ), Lens::isoParsedQuery(), Obj::lensProp( 'lang' ) );

		$this->assertEquals(
			$getData( $langSecondary ),
			Obj::over( $lensOnQueryLang, Fns::always( $langSecondary ), $getData( $langDefault ) )
		);
	}
}
