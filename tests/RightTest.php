<?php

namespace WPML\FP;

use PHPUnit\Framework\TestCase;

class RightTest extends TestCase {

	/**
	 * @test
	 */
	public function it_maps() {
		$right = Either::right( 'something' );
		$result = $right->map( 'strtoupper' );
		$this->assertSame( 'SOMETHING', $result->get() );
	}

	/**
	 * @test
	 */
	public function getOrElse_returns_the_value() {
		$right = Either::right( 'something' );
		$this->assertEquals( 'something', $right->getOrElse( 'other' ) );
	}

	/**
	 * @test
	 */
	public function orElse_is_a_noop() {
		$right = Either::right( 'something' );
		$this->assertSame( $right, $right->orElse( 'strtoupper' ) );
	}

	/**
	 * @test
	 */
	public function chain_applies_the_function_and_unwraps_it() {
		$right = Either::right( 'something' );
		$fn = function( $data ) {
			return Either::right( Either::right( strtoupper($data) ) );
		};
		$this->assertEquals( Either::right('SOMETHING'), $right->chain( $fn ) );
	}

	/**
	 * @test
	 */
	public function getOrElseThrow_gets_the_value() {
		$throwValue = 'error';

		$right = Either::right( 'something' );
		$this->assertEquals( 'something', $right->getOrElseThrow( $throwValue ) );
	}

	/**
	 * @test
	 */
	public function it_has_applicative() {
		$string = 'test';

		$right = Either::right( 'strtoupper' );
		$this->assertEquals( strtoupper( $string ), $right->ap( Either::of( $string ) )->get() );
	}

}
