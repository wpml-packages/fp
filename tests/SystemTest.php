<?php

namespace WPML\FP\System;

use PHPUnit\Framework\TestCase;
use WPML\FP\Fns;
use WPML\FP\Logic;

class SystemTest extends TestCase {

	/**
	 * @test
	 */
	public function it_gets_post_data() {
		$_POST = [ 'data' => 'one', 'other' => 2 ];

		$postDataGetter = System::getPostData();
		$postData       = $postDataGetter()->get();

		foreach ( $_POST as $key => $value ) {
			$this->assertEquals( $value, $postData->get( $key ) );
		}
	}

	/**
	 * @test
	 */
	public function it_filters_collection_with_function() {
		$key  = 'my-key';
		$data = 'my data';

		$filter = getFilterFor( $key )->using( 'strtoupper' );

		$result = $filter ( wpml_collect( [ $key => $data ] ) );

		$this->assertEquals( strtoupper( $data ), $result->get( $key ) );
	}

	/**
	 * @test
	 */
	public function it_filters_collection_and_returns_default_if_not_available() {
		$key     = 'my-key';
		$default = 'default value';

		$filter = getFilterFor( $key )->using( 'strtoupper' )->defaultTo( $default );

		$result = $filter ( wpml_collect() );

		$this->assertEquals( $default, $result->get( $key ) );
	}

	/**
	 * @test
	 */
	public function it_filters_collection_and_returns_default_via_function_if_not_available() {
		$key       = 'my-key';
		$default   = 'default value';
		$defaultFn = function () use ( $default ) { return $default; };

		$filter = getFilterFor( $key )->using( 'strtoupper' )->defaultTo( $defaultFn );

		$result = $filter ( wpml_collect() );

		$this->assertEquals( $default, $result->get( $key ) );
	}

	/**
	 * @test
	 */
	public function it_filters_collection_and_handles_nullable() {
		$key = 'my-key';

		$default = 'default value';

		$filter = getFilterFor( $key )->using( function ( $value ) { return $value; } )->defaultTo( $default );

		$result = $filter ( wpml_collect( [ $key => false ] ) );
		$this->assertNotEquals( $default, $result->get( $key ) );
		$this->assertSame( false, $result->get( $key ) );

		$result = $filter ( wpml_collect( [ $key => null ] ) );
		$this->assertSame( null, $result->get( $key ) );
	}

	/**
	 * @test
	 */
	public function it_filters_var() {
		$string = '<strong>my string</strong>';

		$filter = filterVar( FILTER_SANITIZE_STRING );

		$this->assertEquals( 'my string', $filter( $string ) );
	}

	/**
	 * @test
	 */
	public function it_sanitizes_string() {
		$string = '<strong>my string</strong>';

		$filter = sanitizeString();

		$this->assertEquals( 'my string', $filter( $string ) );
	}

	/**
	 * @test
	 */
	public function it_validates() {
		$validateData   = getValidatorFor( 'data' )->using( Logic::isNotNull() )
		                                           ->error( 'Is null' );

		$value = wpml_collect( [ 'data' => 'something' ] );
		$result = $validateData( $value );
		$this->assertEquals( $value, $result->get() );

		$value = wpml_collect( [ 'data' => null ] );
		$result = $validateData( $value );
		$result->orElse( function( $data ) {
			$this->assertEquals('Is null', $data );
		});

	}


}
